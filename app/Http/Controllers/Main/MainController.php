<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function privacy()
    {
        return view('aviso/es');
    }


    /*
    *
    *FUNCIONES PARA IRAPUATO
    *    
    */
    public function indexIrapuato()
    {
        return view('irapuato/index');
    }
    public function servicesIrapuato()
    {
        return view('irapuato/services');
    }
    public function habitacionesIrapuato()
    {
        return view('irapuato/rooms');
    }
    public function salonesIrapuato()
    {
        return view('irapuato/salon');
    }
    public function sencillaIrapuato()
    {
        return view('irapuato/simple');
    }
    public function dobleIrapuato()
    {
        return view('irapuato/double');
    }
    public function suitesIrapuato()
    {
        return view('irapuato/suites');
    }
    public function luxuryIrapuato()
    {
        return view('irapuato/luxury');
    }
    public function handIrapuato()
    {
        return view('irapuato/handicap');
    }
    public function contactIrapuato()
    {
        return view('irapuato/contact');
    }
    /*
    *
    *FUNCIONES PARA QUERETARO
    *    
    */
    public function indexQro()
    {
        return view('queretaro/index');
    }
    public function servicesQro()
    {
        return view('queretaro/services');
    }
    public function salonesQro()
    {
        return view('queretaro/salon');
    }
    public function habitacionesQro()
    {
        return view('queretaro/rooms');
    }
    public function contactQro()
    {
        return view('queretaro/contact');
    }
    public function estandarQro()
    {
        return view('queretaro/estandar');
    }
    public function estandarksQro()
    {
        return view('queretaro/estandarks');
    }
    public function suitesQro()
    {
        return view('queretaro/suites');
    }
    public function masterQro()
    {
        return view('queretaro/master');
    }
    public function handQro()
    {
        return view('queretaro/handicap');
    }

    /*
    *
    *FUNCIONES PARA Celaya
    *    
    */
    public function indexCelaya()
    {
        return view('celaya/index');
    }

    /*
    *
    *FUNCIONES PARA GALERIAS
    *    
    */
    public function indexGal()
    {
        return view('galerias/index');
    }
    public function servicesGal()
    {
        return view('galerias/services');
    }
    public function salonesGal()
    {
        return view('galerias/salon');
    }
    public function habitacionesGal()
    {
        return view('galerias/rooms');
    }
    public function contactGal()
    {
        return view('galerias/contact');
    }
    public function estandarGal()
    {
        return view('galerias/sencilla');
    }
    public function dobleGal()
    {
        return view('galerias/doble');
    }
    public function suitesGal()
    {
        return view('galerias/suites');
    }
    /*
    *
    *FUNCIONES PARA VELEROS
    *    
    */
    public function indexVel()
    {
        return view('veleros/index');
    }
    public function servicesVel()
    {
        return view('veleros/services');
    }
    public function salonesVel()
    {
        return view('veleros/salon');
    }
    public function habitacionesVel()
    {
        return view('veleros/rooms');
    }
    public function contactVel()
    {
        return view('veleros/contact');
    }
    public function estandarVel()
    {
        return view('veleros/estandar');
    }
    public function juniorVel()
    {
        return view('veleros/junior');
    }
    public function suitesVel()
    {
        return view('veleros/suites');
    }
}
