<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Main page
Route::get('/', function () {
    return view('index');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/privacy', 'Main\MainController@privacy');

// Authentication Routes
Route::get('beadmin', 'Auth\AuthController@showLoginForm');
Route::post('beadmin', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');

//Admin
Route::get('/dashboard', 'Admin\AdminController@index');

//Irapuato
Route::get('/irapuato','Main\MainController@indexIrapuato');
Route::get('/services-irapuato','Main\MainController@servicesIrapuato');
Route::get('/rooms-irapuato','Main\MainController@habitacionesIrapuato');
Route::get('/salon-irapuato','Main\MainController@salonesIrapuato');
Route::get('/contact-irapuato','Main\MainController@contactIrapuato');

Route::get('/simple-irapuato','Main\MainController@sencillaIrapuato');
Route::get('/double-irapuato','Main\MainController@dobleIrapuato');
Route::get('/suite-irapuato','Main\MainController@suitesIrapuato');
Route::get('/luxury-irapuato','Main\MainController@luxuryIrapuato');
Route::get('/handicap-irapuato','Main\MainController@handIrapuato');

//Queretaro
Route::get('/queretaro','Main\MainController@indexQro');
Route::get('/services-queretaro','Main\MainController@servicesQro');
Route::get('/rooms-queretaro','Main\MainController@habitacionesQro');
Route::get('/salon-queretaro','Main\MainController@salonesQro');
Route::get('/contact-queretaro','Main\MainController@contactQro');

Route::get('/deluxematrimonial-queretaro','Main\MainController@estandarQro');
Route::get('/deluxeking-queretaro','Main\MainController@estandarksQro');
Route::get('/suites-queretaro','Main\MainController@suitesQro');
Route::get('/master-queretaro','Main\MainController@masterQro');
Route::get('/handicap-queretaro','Main\MainController@handQro');

//Celaya
Route::get('/celaya','Main\MainController@indexCelaya');

//Galerias
Route::get('/galerias','Main\MainController@indexGal');
Route::get('/services-galerias','Main\MainController@servicesGal');
Route::get('/rooms-galerias','Main\MainController@habitacionesGal');
Route::get('/salon-galerias','Main\MainController@salonesGal');
Route::get('/contact-galerias','Main\MainController@contactGal');

Route::get('/deluxematrimonial-galerias','Main\MainController@dobleGal');
Route::get('/deluxeking-galerias','Main\MainController@estandarGal');
Route::get('/suites-galerias','Main\MainController@suitesGal');

//Veleros
Route::get('/veleros','Main\MainController@indexVel');
Route::get('/services-veleros','Main\MainController@servicesVel');
Route::get('/rooms-veleros','Main\MainController@habitacionesVel');
Route::get('/salon-veleros','Main\MainController@salonesVel');
Route::get('/contact-veleros','Main\MainController@contactVel');

Route::get('/deluxematrimonial-veleros','Main\MainController@estandarVel');
Route::get('/juniorsuite-veleros','Main\MainController@juniorVel');
Route::get('/suites-veleros','Main\MainController@suitesVel');

