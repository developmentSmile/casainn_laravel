 /*================================================
* Template Name: Zante Hotel - Hotel & Resort HTML Template
* Version: 1.2
* Author Name: Jomin Muskaj
* Author URI: eagle-themes.com
=================================================*/

(function ($) {
    "use strict";

    /*========== LOADING PAGE ==========*/
    $(window).on('load', function () {
        $("#loading").fadeOut(500);
    });

    
    /*Document is Raedy */
    $(document).ready(function () {

        /*========== SMOOTHSTATE ==========*/
        $('#smoothpage').smoothState({ 
            anchors: 'nav a',
            blacklist: 'form',                         
        });


        
        $(function () {
            function toggleNavbarMethod() {
                if ($(window).width() > 992) {
                    
                    $('.dropdown')
                        .on('mouseover', function () {
                            $(this).addClass('open');
                            $('b', this).toggleClass("caret caret-up");
                        })

                    .on('mouseout', function () {
                        $(this).removeClass('open');
                        $('b', this).toggleClass("caret caret-up");
                    });


                } else {
                    $('.dropdown').off('mouseover').off('mouseout');
                    $('.dropdown-toggle')

                    .on('click', function (e) {
                        $('b', this).toggleClass("caret caret-up");
                    });

                }
            }
            toggleNavbarMethod();
            $(window).on("resize", (toggleNavbarMethod));

            $(".navbar-toggle").on("click", function () {
                $(this).toggleClass("active");
            });
        });

        /*========== MOBILE MENU ==========*/
        $('.mobile_menu_btn').jPushMenu({
            closeOnClickLink: false
        });
        $('.dropdown-toggle').dropdown();

        
        /*========== REVOLUTION SLIDER ==========*/

        /* ----- Home Page 2 ----- */
        if ($("#classic_slider").length > 0) {
            var tpj = jQuery;
            tpj.noConflict();
            var revapi6;
            tpj(document).ready(function () {
                if (tpj("#classic_slider").revolution == undefined) {
                    revslider_showDoubleJqueryError("#classic_slider");
                } else {
                    revapi6 = tpj("#classic_slider").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "js/",
                        sliderLayout: "auto",
                        dottedOverlay: "none",
                        delay: 2000,
                        navigation: {
                            keyboardNavigation: "on",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "on",
                            touch: {
                                touchenabled: "on",
                                swipe_threshold: 75,
                                swipe_min_touches: 50,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            },
                            arrows: {
                                style: "hermes",
                                enable: true,
                                hide_onmobile: true,
                                hide_under: 600,
                                hide_onleave: true,
                                tmp: '<div class="tp-arr-allwrapper"><div class="tp-arr-imgholder"></div>',
                                left: {
                                    h_align: "left",
                                    v_align: "center",
                                    h_offset: 0,
                                    v_offset: 0
                                },
                                right: {
                                    h_align: "right",
                                    v_align: "center",
                                    h_offset: 0,
                                    v_offset: 0
                                }
                            }

                        },
                        responsiveLevels: [1200, 992, 768, 480],
                        visibilityLevels: [1200, 992, 768, 480],
                        gridwidth: [1200, 992, 768, 480],
                        gridheight: [550, 550, 500, 500],
                        lazyType: "none",
                        parallax: {
                            type: "scroll",
                            origo: "slidercenter",
                            speed: 2000,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 55],
                        },
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "on",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }
            });
        };
        
        
        /*========== COUNT UP ==========*/
        var options = {
            useEasing: true,
            useGrouping: false,
            separator: ',',
            decimal: '.',
            prefix: '',
            suffix: ''
        };

        $.each($('.countup'), function () {
            var count = $(this).data('count'),
                numAnim = new CountUp(this, 0, count);

            numAnim.start();
        });


        /*========== AWESOME FEATURESS ==========*/
        var sliderId = $('#features_slider'); // Slider ID
        sliderId.owlCarousel({
            thumbs: true,
            thumbsPrerendered: true,
            items: 1,
            loop: true,
            autoplay: true,
            dots: false,
            nav: false,
        });

        
        /*========== GALLERY SLIDER ==========*/
        var owl = $('#gallery_slider');
        owl.owlCarousel({
            loop: false,
            nav: false,
            margin: 50,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                960: {
                    items: 3
                },
                1200: {
                    items: 3
                }
            }
        });

        /*========== ROOM SLIDER ==========*/
        var $sync1 = $("#slider-larg"),
            $sync2 = $("#thumbs"),
            duration = 300;

        $sync1.owlCarousel({
                items: 1,
                dots: false,
                nav: true,
                loop:true,
                navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
            })
            .on('changed.owl.carousel', function (e) {
                var syncedPosition = syncPosition(e.item.index);

                if (syncedPosition != "stayStill") {
                    $sync2.trigger('to.owl.carousel', [syncedPosition, duration, true]);
                }
            });

        $sync2
            .on('initialized.owl.carousel', function () {
                addClassCurrent(0);
            })
            .owlCarousel({
                dots: false,
                responsive: {
                    0: {
                        items: 4
                    },
                    600: {
                        items: 4
                    },
                    960: {
                        items: 5
                    },
                    1200: {
                        items: 6
                    }
                }
            })
            .on('click', '.owl-item', function () {
                $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
            });

        function addClassCurrent(index) {
            $sync2
                .find(".owl-item")
                .removeClass("active-item")
                .eq(index).addClass("active-item");
        }
        function syncPosition(index) {
            addClassCurrent(index);
            var itemsNo = $sync2.find(".owl-item").length; //total items
            var visibleItemsNo = $sync2.find(".owl-item.active").length; //visible items

            if (itemsNo === visibleItemsNo) {
                return "stayStill";
            }
            var visibleCurrentIndex = $sync2.find(".owl-item.active").index($sync2.find(".owl-item.active-item"));

            if (visibleCurrentIndex == 0 && index != 0) {
                return index - 1;
            }
            if (visibleCurrentIndex == (visibleItemsNo - 1) && index != (itemsNo - 1)) {
                return index - visibleItemsNo + 2;
            }
            return "stayStill";
        }

        /*========== ROOMS LIST - OWL CAROUSEL ==========*/
        var owl = $('.room_list_slider');
        owl.owlCarousel({
            items: 1,
            loop: true,
            dots: false,
            nav: true,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ]
        });

        /*========== DATE PICKER ==========*/
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd", //Set Date Format
            startDate: new Date(), //Set Min Date Today
            endDate: "2020-12-18", //Set Max Date
            datesDisabled: [], //Set Disabled Dates
            autoclose: true,
            todayHighlight: true,
            allowInputToggle: true,
        });

        /*========== SELECT PICKER ==========
        $('select').selectpicker({
            style: 'btn-select',
            size: 'auto',
            container: 'body',
        });
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
            $('select').selectpicker('mobile');
        }*/


        /*========== CONTACT FORM ==========*/
        $("#contact-form, #contact-form-page").on('submit', function (e) {
            e.preventDefault();

            //Get input field values from HTML form
            var user_name = $("input[name=name]").val();
            var user_phone = $("input[name=phone]").val();
            var user_email = $("input[name=email]").val();
            var user_subject = $("input[name=subject]").val();
            var user_message = $("textarea[name=message]").val();

            //Data to be sent to server
            var post_data;
            var output;
            post_data = {
                'user_name': user_name,
                'user_email': user_email,
                'user_message': user_message,
                'user_phone': user_phone,
                'user_subject': user_subject
            };

            //Ajax post data to server
            $.post('email/email.php', post_data, function (response) {

                //Response server message
                if (response.type == 'error') {
                    output = '<div class="notification error"><span class="notification-icon"><i class="fa fa-exclamation" aria-hidden="true"></i></span><span class="notification-text">' + response.text + '</span></div>';
                } else {
                    output = '<div class="notification success"><span class="notification-icon"><i class="fa fa-check" aria-hidden="true"></i></span><span class="notification-text">' + response.text + '</span></div>';

                    //If success clear inputs
                    $("input, textarea").val('');
                }

                $("#notification").html(output); 

                $(".notification").delay(15000).queue(function (next) {
                    $(this).addClass("scale-out");
                    next();
                });
                $(".notification").on("click", function(){ 
                    $(this).addClass("scale-out");
                });

            }, 'json');
        });

        
        /*========== MAGNIFIC POPUP ==========*/
        $(".magnific-popup, a[data-rel^='magnific-popup']").magnificPopup({
            type: 'image',
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below

            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out',
                fixedContentPos: true,
                opener: function (openerElement) {
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            },

            retina: {
                ratio: 1, // Increase this number to enable retina image support.
                replaceSrc: function (item, ratio) {
                    return item.src.replace(/\.\w+$/, function (m) {
                        return '@2x' + m;
                    });
                }
            }

        });

        $('.image-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            fixedContentPos: true,
            gallery: {
                enabled: true
            },
            removalDelay: 300,
            mainClass: 'mfp-fade',
            retina: {
                ratio: 1,
                replaceSrc: function (item, ratio) {
                    return item.src.replace(/\.\w+$/, function (m) {
                        return '@2x' + m;
                    });
                }
            }

        });

        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 300,
            preloader: false,
            fixedContentPos: true,
        });

        /*========== POP OVER & TOOLTIP ==========*/
        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip({
           animated: 'fade', 
           container: 'body'
        });

        

        /*========== BACK TO TOP ==========*/
        var amountScrolled = 500;
        var back_to_top = $('#back_to_top');
        $(window).on('scroll', function () {
            if ($(window).scrollTop() > amountScrolled) {
                back_to_top.addClass('active');
            } else {
                back_to_top.removeClass('active');
            }
        });
        back_to_top.on('click', function () {
            $('html, body').animate({
                scrollTop: 0
            }, 500);
            return false;
        });


        $('.sendbooking').click(function(){ 
                var urlqueretaro='https://secure.internetpower.com.mx/portals/CasaInnQueretaro/hotel/hoteldescription.aspx';
                var urlgalerias='https://secure.internetpower.com.mx/portals/CasaInnGalerias/hotel/hoteldescription.aspx';
                var urlirapuato='https://secure.internetpower.com.mx/portals/CasaInnIrapuato/hotel/hoteldescription.aspx';
                var urlcelaya='https://secure.internetpower.com.mx/portals/CasaInnCelaya/hotel/hoteldescription.aspx';  
                
                var checkin=$('#checkin').val();
                var checkout=$('#checkout').val();
                var hotel=$('#PropertyNumber').val();
                
                if(hotel==6486){
                    $('#booking-form').attr('action',urlqueretaro);
                }
                if(hotel==6487){
                    $('#booking-form').attr('action',urlgalerias);
                }
                if(hotel==6488){
                    $('#booking-form').attr('action',urlirapuato);
                }
                if(hotel==6489){
                    $('#booking-form').attr('action',urlcelaya);
                }
                
                checkin=checkin.replace(/\-/g,'');
                checkout=checkout.replace(/\-/g,'');
                $('#checkin').val(checkin);
                $('#checkout').val(checkout);
                
                $('#booking-form').submit();
                
            });

    });
})(jQuery);