<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Irapuato Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during irapuato for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'titulobienvenido'      =>  'Bienvenido a Casa Inn Business Hotel Irapuato',
    'textobienvenido'       =>  'Donde el placer y el servicio se unen en una ciudad icónica de Guanajuato',
    'titulogaleria'         =>  'Galería',
    'titulohotelcuenta'     =>  'El Hotel cuenta con:',
    'alberca'               =>  'Alberca climatizada.',
    'gimnasio'              =>  'Gimnasio completo.',
    'salones'               =>  'Cuatro salones privados para reuniones de trabajo, totalmente equipados para facilitar sus reuniones, grupos y eventos de hasta 100 personas. *',
    'salajuntas'            =>  'Sala de juntas de nivel ejecutivo, que incluye lo más moderno en equipo de telecomunicación.',
    'areanegocios'          =>  'Área de negocios con computadoras y acceso a internet de uso gratuito.',
    'tituloservicioshotel'  =>  'Servicios que ofrece el Hotel dentro de la ciudad:',
    'acceso'                =>  'Acceso a plazas y áreas de interés de Irapuato.',
    'divisas'               =>  'Cambio de divisas.',
    'lavanderia'            =>  'Servicio de lavandería y tintorería. *',
    'transportacion'        =>  'Transportación en cortesía en un radio de 10 km. Previa solicitud y disponibilidad.',
    'costoextra'            =>  'Costo extra.',
    'restauranteterraza'    =>  'Restaurante La Terraza, ofreciendo desayuno tipo americano y buffet caliente.',
    'cenas'                 =>  'Cenas Ligeras. *',
    'restaurantebeef'       =>  'Restaurante Beef Capital con servicio de comidas y cenas. *',
    'serviciolavanderia'    =>  'Área con servicio de lavandería B2C. *',
    'bar'                   =>  'Bar para disfrutar cocteles y bebidas. *',
    'playstate'             =>  'Introduciendo el Casa Inn PlayState (zona de esparcimiento con terraza).',
    'servicios'             =>  'SERVICIOS',
    'listahab'              =>  'LISTA DE HABITACIONES',
    'reservar'              =>  'RESERVAR',
    'habitacion'            =>  'Habitación',
    'restaurante'           =>  'Restaurante',
    'salajuntas'            =>  'Sala de juntas',
    'salon'                 =>  'Salón',
    'sofacama'              =>  'Sofá cama.',
    'sencilla'              =>  'Deluxe con cama King Size',
    'dessencilla'           =>  'Te invitamos a disfrutar de la comodidad de nuestra habitación Deluxe con cama King Size y sofá cama.',
    'doble'                 =>  'Deluxe con 2 camas matrimoniales',
    'desdoble'              =>  'Descansa en nuestras habitaciones Deluxe con dos camas matrimoniales, en donde se une la elegancia con lo práctico.',
    'stay'                  =>  'Suite Stay',
    'desstay'               =>  'Disfruta de una estancia llena de comodidades y amplios espacios.',
    'luxury'                =>  'Suite Luxury',
    'desluxury'             =>  'Nuestras Suites Luxury te brindarán el confort que necesitas para disfrutar de una estadía inolvidable.',
    'aire'                  =>  'Aire acondicionado.',
    'plancha'               =>  'Plancha y tabla de planchar.',
    'secadora'              =>  'Secadora de cabello.',
    '30m2'                  =>  '30m2.',
    'frigobar'              =>  'Frigobar.',
    'wifi'                  =>  'Acceso a WIFI gratuito.',
    'radio'                 =>  'Radio / reloj despertador.',
    'caja'                  =>  'Caja de seguridad.',
    'cafetera'              =>  'Cafetera.',
    'pantalla'              =>  'Pantalla Smart TV.',
    'canales'               =>  '40 canales por cable incluido 1 canal Japones.',
    'tina'                  =>  'Tina.',
    /*HABITACION SENCILLA*/
    'textosencilla'         =>  'Casa Inn Business Hotel Irapuato es un hotel con diseño moderno uniendo la elegancia con lo práctico. Cuenta con 24 habitaciones Deluxe con cama King Size y con un amplio espacio ofreciendo así el punto más alto de confort.',
    'serviciossencilla'     =>  'SERVICIOS QUE OFRECE LA HABITACIÓN DELUXE CON CAMA KING SIZE:',
    'mesasillas'            =>  'Escritorio de trabajo con asiento ergonómico',
    /*HABITACION DOBLE*/
    'textodoble'            =>  'Casa Inn Business Hotel Irapuato es un hotel con diseño moderno uniendo la elegancia con lo práctico. Cuenta con 64 habitaciones Deluxe con 2 camas matrimoniales con un amplio espacio ofreciendo así el punto más alto de confort, y una habitación para personas con capacidades diferentes.',
    'serviciosdoble'        =>  'SERVICIOS QUE OFRECE LA HABITACIÓN DELUXE CON 2 CAMAS MATRIMONIALES:',
    /*HABITACION STAY SUITES*/
    'textostay'             =>  'Casa Inn Business Hotel Irapuato es un hotel con diseño moderno uniendo la elegancia con lo práctico. Cuenta con 16 Suites Stay con un amplio espacio ofreciendo así el punto más alto de confort. Nuestras habitaciones Suites Stay están equipadas con refrigerador, microondas y cocineta.',
    'serviciosstay'         =>  'SERVICIOS QUE OFRECE LA HABITACIÓN SUITE STAY:',
    '37m2'                  =>  '37m2.',
    'refrigerador'          =>  'Refrigerador.',
    'microondas'            =>  'Microondas.',
    'cocineta'              =>  'Cocineta completa.',
    'sofaCama'              =>  'Sofá cama.',
    'sala'                  =>  'Sala recibidor con pantalla TV.',
    'tina'                  =>  'Tina de baño.',
    'plaque'                =>  'Plaqué y utensilios de cocina completa.',
    'tarja'                 =>  'Tarja.',
    '2tv'                   =>  '2 Smart TV.',
    /*HABITACION LUXURY*/
    'textoluxury'           =>  'Casa Inn Business Hotel Irapuato es un hotel con diseño moderno uniendo la elegancia con lo práctico. Cuenta con 16 Suites Luxury con un amplio espacio de 37 m2 ofreciendo así el punto más alto de confort. Nuestras habitaciones Suites Luxury están equipadas con refrigerador, cocineta completa, microondas, plaqué y utensilios de cocina completos.',
    'serviciosluxury'        =>  'SERVICIOS QUE OFRECE LA HABITACIÓN SUITE LUXURY:',
    /*HABITACION HANDICAP*/
    'handicap'              =>  'Handicap (discapacitados)',
    'textohan'              =>  'En Casa Inn Premium Hotel Querétaro podrá disfrutar además de nuestro gran servicio, la comodidad y modernidad de 1 habitación con dos camas individuales',
    'serviciohan'           =>  'SERVICIOS QUE OFRECE LA HABITACIÓN HANDICAP: ',
    /*CONTACTO*/
    'contactanos'           =>  'CONTÁCTANOS',
    'nombre'                =>  'Nombre',
    'telefono'              =>  'Teléfono',
    'correo'                =>  'Correo',
    'asunto'                =>  'Asunto',
    'mensaje'               =>  'Mensaje',
    'direccion'             =>  'DIRECCIÓN',
    'enviar'                =>  'ENVIAR',
    'datos'                 =>  'MAS INFORMACIÓN',
    /*SALONES*/
    'irapuatosalon'  => 'Irapuato 1 y 2',
    'irapuatodes'  => 'Ideal para tus reuniones ejecutivas o eventos personales.',
    'irapuatomont'  => 'Montajes: Escuela (24 personas), Auditorio (60), Herradura (30), Coctel (70), Banquete (40), Mesa Imperial (36), Mesa Rusa (36).',
    'irapuato12'  => 'Irapuato 1 y 2 (juntos)',
    'ir12des'  => 'Ideal para: Bodas, XV años, Graduaciones, Bautizos, etc, así como para sus Congresos, Convenciones, Conferencias Expos y reuniones de trabajo.',
    'ir12mont'  => 'Montajes: Escuela (48 personas), Auditorio (120), Herradura (50), Coctel (140), Banquete (80), Mesa Imperial (70), Mesa Rusa (70).',
    'bajio12' => 'Bajio 1 y 2',
    'bajio12des' => 'Ideal para reuniones de trabajo.',
    'bajio12montaje' => 'Montajes: Escuela (12 personas), Auditorio (30), Herradura (15), Coctel (30), Mesa Imperial (18), Mesa Rusa (18).',
    'bajio12juntos' => 'Bajio 1 y 2 (juntos)',
    'bajio12desjuntos' => 'Ideal para Sesiones de Trabajo, Talleres y Break Outs.',
    'bajio12montajejuntos' => 'Montajes: Escuela (24 personas), Auditorio (60),Banquete (40), Herradura (30), Coctel (60), Mesa Imperial (33), Mesa Rusa (33).',
    'gob' => 'Sala Ejecutiva Gobernador',
    'gobdes' => 'Disfruta de tus eventos personales y laborales en nuestro salon Gobernador.',
    'gobmontaje' => 'Montaje fijo Mesa Imperial (12 personas)',


];
