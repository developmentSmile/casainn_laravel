<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Manu Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the principal menu.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'bienvenido'    =>  'Bienvenido',
    'destinos'      =>  'Destinos',
    'eventos'       =>  'Eventos',
    'contactanos'   =>  'Contáctanos',
    'reserva'       =>  'Reserva ahora',
    'servicios'     =>  'Servicios',
    'habitaciones'  =>  'Habitaciones',
    'salones'       =>  'Salones',
    'cotiza'        =>  'Contáctanos',
    'habanner'      =>  'Lista de habitaciones',

];
