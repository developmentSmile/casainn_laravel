<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Footer Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during footer for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'textoservclientes' =>  'Servicio a clientes',
    'destinos'          =>  'Destinos',
    'facturacion'       =>  'Facturación',
    'mediamanager'      =>  'Media Manager',
    'blog'              =>  'Blog',
    'titulopoliticas'   =>  'Políticas',
    'textopoliticas'    =>  'Políticas de privacidad de Casa Inn Hoteles',
    'titulotelefonos'   =>  'Teléfonos',
    'tituloreserv'      =>  'Reservaciones en línea',

];
