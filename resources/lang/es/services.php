<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Services Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during services for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'desayunoamericano' =>  'Desayuno tipo Buffet Americano.',
    'desayuno'          =>  'Desayuno Buffet.',
    'terraza'           =>  'Terraza con chimenea.',
    'terraza2'          =>  'Terraza.',
    'bar'               =>  'Bar.',
    'roomservice'       =>  'Room Service 24 hrs.',
    'gym'               =>  'Gym.',
    'nado'              =>  'Canal de nado techado y climatizado.',
    'nadoaire'          =>  'Canal de nado climatizado.',
    'personal'          =>  'Personal Bilingüe.',
    'canal'             =>  'Canal Japonés.',
    'lavanderia'        =>  'Servicio de Lavandería y Tintorería.',
    'lavado'            =>  'Autoservicio de lavado y secado.',
    'internet'          =>  'Internet de alta velocidad.',
    'negocios'          =>  'Centro de negocios.',
    'transporte'        =>  'Transportación sujeta a disponibilidad.',
    'incendio'          =>  'Sistema contra incendio.',
    'estacionamiento'   =>  'Estacionamiento gratuito.',
    'alberca'           =>  'Alberca climatizada.',
    'computadoras'      =>  'Área de negocios con computadoras.',
    'cenas'             =>  'Cenas ligeras.',
    'playstate'         =>  'PlayState.',
    'restaurante'       =>  'Restaurante con cocina Nacional e Internacional.',
    'ludoteca'          =>  'Ludoteca.',
    'vitrina'           =>  'Vitrina para snack.',
    'computo'           =>  'Área de computo.',
    'caja'              =>  'Caja de seguridad en todas las habitaciones.',
    'cunas'             =>  'Servicio de cunas.',
    'camaras'           =>  'Cámaras de videovigilancia.',
    'discapacidad'      =>  'Facilidades para personas con discapacidad.',
    'sky'               =>  'Sky Lounge.',
    'chapoteadero'      =>  'Chapoteadero.',
    'vapor'             =>  'Vapor.',
    'sauna'             =>  'Sauna.',
    'jacuzzi'           =>  'Jacuzzi.',
    'wifi'              =>  'WiFi gratuito en todas las áreas del hotel.',
    'playstate2'        =>  'PlayState con Smart TV y 60 canales japoneses.',    
    'costoextra'        =>  '* Costo Extra.',    
    'discapacitados'    =>  'Habitación para discapacitados.',    
];
