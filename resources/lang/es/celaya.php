<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Celaya Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during celaya for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'bienvenido' => '¡Es un gusto poderle ofreces el mejor de los servicios en nuestras dos ubicaciones!',
    'titulogaleria' => 'Galería',
    
];
