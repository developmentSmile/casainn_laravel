<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the principal page.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'selecthotel'       =>  'Selecciona un hotel',
    'llegada'           =>  'Fecha de Llegada',
    'salida'            =>  'Fecha de Salida',
    'habitacion'        =>  'Habitaciones',
    'adultos'           =>  'Adultos',
    'ninos'             =>  'Menores (0 - 17)',
    'buscar'            =>  'Buscar',
    'tituloacerca'      =>  'Acerca de Casa Inn',
    'textoacerca'       =>  '',
    'placer'            =>  'Nuestro Placer es Atenderle',
    'bienvenidoconcepto'=>  'Nuevo concepto de hoteles Premium & Business Class.',
    'tituloservicios'   =>  'Nuestros Servicios',
    'textoservicios'    =>  'En visitas de placer o negocios le ofrecemos el más alto servicio, y amenidades de primera clase.',
    'textoservicios2'   =>  'Relájese en cualquiera de nuestras albercas, realice sus reuniones en nuestra salas de juntas o centros de negocios o planee un evento en nuestra variedad de salones.',
    'restaurante'       =>  'Restaurante',
    'areas'             =>  'Áreas recreativas',
    'salas'             =>  'Salas de junta',
    'alberca'           =>  'Alberca',
    'titulolugares'     =>  'Lugares ideales para disfrutar',
    'botonlugares'      =>  'Encuentra tu hotel',
    'eventos'           =>  'Eventos',
    'titulobodas'       =>  'Bodas y eventos',
    'textobodas'        =>  'Recuerdos entrañables que se conservarán para siempre...',
    'titulovacaciones'  =>  'Vacaciones',
    'textovacaciones'   =>  'Hoteles con fácil acceso a emocionantes atracciones, paisajes históricos, programas familiares, aventura, cultura, local, etc...',
    'tituloreuniones'   =>  'Reuniones',
    'textoreuniones'    =>  'El principal objetivo de estos hoteles es transformar los viajes de negocios en un universo de comodidades y conveniencias...',
];
