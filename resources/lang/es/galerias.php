<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Galerias Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during galerias for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'bienvenido'        =>  'BIENVENIDOS A CASA INN BUSINESS HOTEL GALERÍAS CELAYA',
    'placer'            =>    'Donde el placer y el servicio se unen en una ciudad icónica de Guanajuato ',
    'titulogaleria'     =>  'Galería',
    //SERVICIOS
    'titulohotelcuenta' =>  'El Hotel cuenta con:',
    'estacionamiento'   =>  'Amplio estacionamiento gratuito',
    'desayuno'          =>  'Desayuno tipo buffet americano (06:00 – 11:00hrs)',
    'terraza'           =>  'Restaurante la Terraza, con menú de cena “3 de 3”, 3 ensaladas, 3 entradas, 3 platos fuertes (18:00 – 23:00 hrs.)',
    'bar'               =>  'Bar',
    'roomservice'       =>  'Room Service',
    'canalnado'         =>  'Canal de nado techado y climatizado',
    'salonescap'        =>  'Salones con capacidad desde 12 pax hasta 50 pax',
    'centroneg'         =>  'Centro de negocios',
    'transportacion'    =>  'Transportación sujeta a disponibilidad',
    'incendiocomp'      =>  'Sistema contra incendio computarizado',
    'bilingue'          =>  'Personal bilingüe',
    'canaljap'          =>  'Canal Japonés (NHK)',
    'servlav'           =>  'Servicio de Lavandería y Tintorería',
    'autolav'           =>  'Autoservicio de lavado y secado',
    'internet'          =>  'Internet de alta velocidad',
    'gym'               =>  'Gym (24 hrs.)',
    //HABITACIONES
    'estandar'          =>  'Deluxe con cama King Size',
    'doble'             =>  'Deluxe con 2 camas matrimoniales',
    'suites'            =>  'Suite',
    'dessuites'         =>  'La habitación suite cuenta con amplios espacios que te permitirán  disfrutar de una estancia larga.',
    'desestandar'       =>  'Te invitamos a disfrutar de la comodidad de nuestra habitación deluxe con cama King Size.',
    'desdoble'          =>  'Disfruta de una habitación llena de lujos y comodidades.',
    'reservar'          =>  'Reservar',
    'textoestandar'     =>  'En Casa Inn Business Hotel Celaya Galerías podrá disfrutar además de nuestro gran servicio, la comodidad y modernidad de 40 habitaciones Deluxe con cama King Size. En un amplio espacio ofreciendo así el punto más alto de confort. ',
    'serviciosastandar'  =>  'SERVICIOS QUE OFRECE LA HABITACIÓN DELUXE CON CAMA KING SIZE:',
    'textodoble'  =>  'En Casa Inn Business Hotel Celaya Galerías podrá disfrutar además de nuestro gran servicio, la comodidad y modernidad de 48 habitaciones Deluxe con 2 camas matrimoniales. En un amplio espacio ofreciendo así el punto más alto de confort.',
    'serviciosdoble'  =>  'SERVICIOS QUE OFRECE LA HABITACIÓN DELUXE CON 2 CAMAS MATRIMONIALES:',
    'textosuites'  =>  'En Casa Inn Business Hotel Celaya Galerías podrá disfrutar además de nuestro gran servicio, la comodidad y modernidad de 32 habitaciones Suite para hospedaje extendido.',
    'serviciossuites'  =>  'SERVICIOS QUE OFRECE LA HABITACIÓN SUITE:',
    //SERVICIOS POR HAB
    'smart'  =>  'Smart tv.',
    'frigobar'  =>  'Frigobar.',
    'seguridad'  =>  'Caja de seguridad digital.',
    'planchado'  =>  'Kit de planchado.',
    'cafe'  =>  'Cafetera.',
    'almohadas'  =>  'Menú de almohadas (suave y firme).',
    'aire'  =>  'Aire acondicionado.',
    'escritorio'  =>  'Escritorio de trabajo con asiento ergonómico.',
    'despertador'  =>  'Reloj despertador.',
    'canales'  =>  'Más de 70 canales por cable.',
    'ventanas'  =>  'Ventanas aislantes de ruido y temperatura.',
    'lamparas'  =>  'Lámparas de lectura LED.',
    'telefono'  =>  'Teléfono.',
    'hielera'  =>  'Hielera.',
    'ganchos'  =>  'Ganchos normales.',
    'pinzas'  =>  'Ganchos con pinzas.',
    'tina'  =>  'Tina de baño.',
    'regadera'  =>  'Regadera de cielo.',
    'baño'  =>  'Kit de baño.',
    'secadora'  =>  'Secadora de cabello.',
    'incendio'  =>  'Sistema contra incendio.',
    'puertas'  =>  'Puerta retardante al fuego.',
    'chapas'  =>  'Chapas de proximidad con mirilla.',
    'energia'  =>  'Sistema de ahorro de energía.',
    'medidas'  =>  'Medidas: 6.80 mts de largo por 4 mts de ancho',
    'medidasdoble'  =>  '6.60 mts de largo por 3.80 mts de ancho',
    'medidassuites'  =>  'Medidas: 8.60 mts de largo por 4 mts de ancho.',
    'cocineta'  =>  'Cocineta.',
    'barra'  =>  'Barra de desayunador con 2 sillas altas para barra.',
    'parrilla'  =>  'Parrilla eléctrica.',
    'campana'  =>  'Campana.',
    'refri'  =>  'Refrigerador.',
    'utensilios'  =>  'Cocina completamente equipada para 4 personas.',
    'futon'  =>  'Futon.',
    'wifi'                  =>  'Wifi en cortesía en todas las áreas del hotel.',
   /*SALONES*/
    'bajio'  =>  'Bajío 1,2,3 y 4',
    'bajiodes'  =>  'Ideal para tus reuniones ejecutivas o eventos personales.Ideal para tus reuniones ejecutivas o eventos personales.',
    'bajiomontaje'  =>  'Montajes: Escuela (12 personas), Auditorio (30), Herradura (16), Coctel (30), Mesa Imperial (20) y Mesa Rusa (18).',
    'bajio12'  =>  'Bajío 1 y 2 (juntos)',
    'bajio12des'  =>  'Ideal para Sesiones de Trabajo, Talleres y Break Outs.',
    'bajio12montaje'  =>  'Montajes: Escuela (24 personas), Auditorio (60), Herradura (30), Coctel (70), Mesa Imperial (40) y Mesa Rusa (26).',
    'bajio5'  =>  'Bajío 5',
    'bajio5des'  =>  'Nuestros salones están diseñados para Congresos, Convenciones, Conferencias, Juntas de Trabajo y Eventos Sociales.',
    'bajio5montaje'  =>  'Montajes: Escuela (22 personas), Auditorio (55), Herradura (28), Coctel (60), Mesa Imperial (30) y Mesa Rusa (24).',
    'sala'  =>  'Sala Especial',
    'salades'  =>  'Disfruta de tus eventos personales y laborales en nuestra Sala Especial.',
    'salamontaje'  =>  'Montaje fijo: Mesa Imperial (12 personas)',

    
];
