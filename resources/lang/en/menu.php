<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Manu Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the principal menu.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'bienvenido'    =>  'Welcome',
    'destinos'      =>  'Destinies',
    'eventos'       =>  'Events',
    'contactanos'   =>  'Contact us',
    'reserva'       =>  'Book now',

];