@extends('layouts.appirapuato')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('irapuato.luxury')}}</h3>
        </div>

    <!-- =========== MAIN ========== -->
        <main id="room_page">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="slider">
                            <div id="slider-larg" class="owl-carousel">
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/irapuato/luxury01.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/irapuato/luxury02.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/irapuato/luxury03.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/irapuato/luxury04.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/irapuato/luxury05.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/irapuato/luxury06.jpg" alt="Image">
                                </div>

                            </div>
                            <div id="thumbs" class="owl-carousel">
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/irapuato/luxury001.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/irapuato/luxury002.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/irapuato/luxury003.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/irapuato/luxury004.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/irapuato/luxury005.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/irapuato/luxury006.jpg" alt="Image"></div>

                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                    
                        <div class="main_title mt50"></div>
                        <p class="text-center">{{ trans('irapuato.textoluxury')}}</p>
                        
                        <div class="main_title t_style a_left s_title mt50">
                            <div class="c_inner">
                                <h2 class="c_title">{{ trans('irapuato.serviciosluxury')}}</h2>
                            </div>
                        </div>
                        <div class="room_facilitys_list">
                            <div class="all_facility_list">
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-building"></i>{{ trans('irapuato.37m2')}}</li>
                                        <li><i class="fa fa-television"></i>{{ trans('irapuato.canales')}}</li>
                                        <li><i class="fa fa-wifi"></i>{{ trans('irapuato.wifi')}}</li>
                                        <li><i class="fa fa-snowflake-o"></i>{{ trans('irapuato.aire')}}</li>
                                        <li><i class="fa fa-coffee"></i>{{ trans('irapuato.cafetera')}}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('irapuato.cocineta')}}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        
                                        <li><i class="fa fa-cutlery"></i>{{ trans('irapuato.microondas')}}</li>
                                        <li><i class="fa fa-television"></i>{{ trans('irapuato.pantalla')}}</li>
                                        <li><i class="fa fa-black-tie"></i>{{ trans('irapuato.plancha')}}</li>
                                        <li id="planos"><i class="fa fa-map"></i>{{ trans('queretaro.planos') }}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('irapuato.plaque')}}</li>
                                        <li><i class="fa fa-music"></i>{{ trans('irapuato.radio')}}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding_left">
                                    <ul class="list-unstyled">
                                        
                                        <li><i class="fa fa-cutlery"></i>{{ trans('irapuato.refrigerador')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('irapuato.sala')}}</li>
                                        <li><i class="fa fa-black-tie"></i>{{ trans('irapuato.secadora')}}</li>
                                        <li><i class="fa fa-bed"></i>{{ trans('irapuato.sofaCama')}}</li>
                                        <li><i class="fa fa-bath"></i>{{ trans('irapuato.tina')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div id="myModal" class="modal">
                            <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
                            <img class="modal-content" id="img01">
                            <div id="caption"></div>
                        </div>

                        <div class="col-md-4 col-md-offset-4 mt40">
                            <a href="/irapuato" class="button  btn_blue btn_full upper">{{ trans('menu.reserva') }}</a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </main>
@endsection

@section('javascripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var d = document.getElementById("rooms");
            d.className += " active";

            /*
             JS Modal
             */
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var text = document.getElementById('planos');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            text.onclick = function(){
                modal.style.display = "block";
                modalImg.src = "hotel/images/irapuato/Irapuato_Junior_Suite_A.jpg";//Nombre Archivo
                captionText.innerHTML = "{{ trans('irapuato.luxury')}}"; //Descripcion imagen
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
                modal.style.display = "none";
            }
        });
    </script>
@endsection