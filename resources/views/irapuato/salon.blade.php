@extends('layouts.appirapuato')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title2">
            <img src="hotel/images/irapuato/salonbanner.jpg" class="img-responsive">
        </div>

    <!-- ========== MAIN ========== -->
        <main id="events_page">
            <div class="container">

                <!-- ITEM -->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 70 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('irapuato.irapuatosalon')}}</h5>
                                <div class="meta">
                                    <div class="time">72 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('irapuato.irapuatodes')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('irapuato.irapuatomont')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/irapuato/salon4.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM 12-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 140 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('irapuato.irapuato12')}}</h5>
                                <div class="meta">
                                    <div class="time">144 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('irapuato.ir12des')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('irapuato.ir12mont')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/irapuato/salon5.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM Bajio 12-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 30 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('irapuato.bajio12')}}</h5>
                                <div class="meta">
                                    <div class="time">36 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('irapuato.bajio12des')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('irapuato.bajio12montaje')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/irapuato/salon1.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM Bajio 12 juntos-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 60 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('irapuato.bajio12juntos')}}</h5>
                                <div class="meta">
                                    <div class="time">36 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('irapuato.bajio12desjuntos')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('irapuato.bajio12montajejuntos')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/irapuato/salon2.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM Bajio gob-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 12 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('irapuato.gob')}}</h5>
                                <div class="meta">
                                    <div class="time">32 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('irapuato.gobdes')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('irapuato.gobmontaje')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/irapuato/salon3.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-4 mt40">
                    <a href="/contact-irapuato" class="button  btn_blue btn_full upper">{{ trans('menu.cotiza') }}</a>
                </div>
            </div>
        </main>

@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("salon");
        d.className += " active";
    });
</script>
@endsection