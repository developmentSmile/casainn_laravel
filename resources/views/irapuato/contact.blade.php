@extends('layouts.appirapuato')

@section('content')
    <!-- =========== GOOGLE MAP ========== -->
        <div id="map">
            <div id="google-map">
            </div>
            <div id="map-canvas"></div>
        </div>

        <!-- ========== MAIN ========== -->
        <main id="contact_page">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-8">
                        <div class="main_title a_left">
                            <h2>{{ trans('irapuato.contactanos')}}</h2>
                        </div>
                        <form id="contact-form-page">
                            <div class="row">
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('irapuato.nombre')}}</label>
                                    <input type="text" class="form-control" name="name" placeholder="{{ trans('irapuato.nombre')}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('irapuato.telefono')}}</label>
                                    <input type="text" class="form-control" name="phone" placeholder="{{ trans('irapuato.telefono')}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('irapuato.correo')}}</label>
                                    <input type="email" class="form-control" name="email" placeholder="{{ trans('irapuato.correo')}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('irapuato.asunto')}}</label>
                                    <input type="text" class="form-control" name="subject" placeholder="{{ trans('irapuato.asunto')}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">{{ trans('irapuato.mensaje')}}</label>
                                    <textarea class="form-control" name="message" placeholder="{{ trans('irapuato.mensaje')}}"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <button type="submit" class="button  btn_blue mt40 upper pull-right">
                                        <i class="fa fa-paper-plane-o" aria-hidden="true"></i> {{ trans('irapuato.enviar')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="main_title a_left">
                            <h2>{{ trans('irapuato.datos')}}</h2>
                        </div>
                        <ul class="contact-info">
                            <li>
                                <span>{{ trans('irapuato.direccion')}}:</span> Av. Villas de Irapuato 621, <br>Colonia 1º de Mayo<br>Irapuato Guanajuato 36644 México
                            </li>
                            <li>
                                <span>{{ trans('irapuato.correo')}}:</span> reservairapuato@casainn.com.mx
                            </li>
                            <li>
                                <span>{{ trans('irapuato.telefono')}}:</span> 01 (462) <strong>119 32 00</strong>
                            </li>
                        </ul>
                        <div class="social_media sm_contact">
                            <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                            <a class="youtube" href="#"><i class="fa fa-youtube"></i></a>
                            <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                            <a class="googleplus" href="#"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("contact");
        d.className += " active";
    });
</script>

<script type="text/javascript">
/*========== GOOGLE MAP ==========*/
        function initialize() {
            var map;
            var panorama;
            //20.678662, -101.375605
            var var_latitude = 20.678662; // Google Map Latitude
            var var_longitude = -101.375605; // Google Map Longitude
            var pin = '../hotel/images/icons/pin.png';

            //Map pin-window details
            var title = "Casa inn - Querétaro";
            var hotel_name = "Casa inn - Querétaro";
            var hotel_address = "Paseo de Miranda Ote 2,Fracc Monte Miranda Querétaro";
            var hotel_desc = "Hotel";

            var hotel_location = new google.maps.LatLng(var_latitude, var_longitude);
            var mapOptions = {
                center: hotel_location,
                zoom: 15,
                scrollwheel: false, 
                streetViewControl: false
            };
            map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
            var contentString =
                '<div id="infowindow_content">' +
                '<p><strong>' + hotel_name + '</strong><br>' +
                hotel_address + '<br>' +
                hotel_desc + '</p>' +
                '</div>';

            var var_infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            var marker = new google.maps.Marker({
                position: hotel_location,
                map: map,
                icon: pin,
                title: title,
                maxWidth: 500,
                optimized: false,
            });
            google.maps.event.addListener(marker, 'click', function () {
                var_infowindow.open(map, marker);
            });
        }

        //Check if google map div exist
        if ($("#map-canvas").length > 0){
           google.maps.event.addDomListener(window, 'load', initialize);
        }
</script>
@endsection