@extends('layouts.appirapuato')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('menu.habanner')}}</h3>
        </div>

    <!-- =========== MAIN ========== -->
        <main id="rooms_list">
           <div class="container">
              <!-- ITEM SENCILLA-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/simple-irapuato" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/irapuato/SENCILLA.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/simple-irapuato"> {{ trans('irapuato.sencilla')}} </a></h3>
                             <p class="text-justify">{{ trans('irapuato.dessencilla')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i> 
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i>
                                <i class="fa fa-cutlery"></i> 
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="#" class="button  btn_blue btn_full upper">{{ trans('irapuato.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>
              <!-- ITEM DOBLE-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/double-irapuato" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/irapuato/DOBLE.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/double-irapuato"> {{ trans('irapuato.doble')}} </a></h3>
                             <p class="text-justify">{{ trans('irapuato.desdoble')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i> 
                                <i class="fa fa-cutlery"></i> 
                                <i class="fa fa-wheelchair"></i> 
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="#" class="button  btn_blue btn_full upper">{{ trans('irapuato.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>
              <!-- ITEM STAY-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/suite-irapuato" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/irapuato/STAY.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/suite-irapuato"> {{ trans('irapuato.stay')}} </a></h3>
                             <p class="text-justify">{{ trans('irapuato.desstay')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i> 
                                <i class="fa fa-cutlery"></i>
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="#" class="button  btn_blue btn_full upper">{{ trans('irapuato.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>
              <!-- ITEM LUXURY-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/luxury-irapuato" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/irapuato/LUXURY.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/luxury-irapuato"> {{ trans('irapuato.luxury')}} </a></h3>
                             <p class="text-justify">{{ trans('irapuato.desluxury')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i> 
                                <i class="fa fa-cutlery"></i>
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="#" class="button  btn_blue btn_full upper">{{ trans('irapuato.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>
               
           </div>
        </main>


@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("rooms");
        d.className += " active";
    });
</script>
@endsection