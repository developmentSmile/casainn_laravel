@extends('layouts.app')

@section('styles')
    <link href="hotel/revolution/css/layers.css" rel="stylesheet" type="text/css" />
    <link href="hotel/revolution/css/settings.css" rel="stylesheet" type="text/css" />
    <link href="hotel/revolution/css/navigation.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
            <!-- ========== REVOLUTION SLIDER ========== -->
            <div id="classic_slider" class="rev_slider" style="display:none">
                <ul>
                    <!-- SLIDE NR. 1 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/corporativo/Slider01.jpg"  
                             alt="Image" 
                             title="slider_bg2"
                             data-bgposition="center center" 
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat" 
                             data-bgparallax="10" 
                             class="rev-slidebg" 
                             data-no-retina="">
                    </li>
                    
                    <!-- SLIDE NR. 2 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/corporativo/Slider02.jpg"  
                             alt="Image" 
                             title="slider_bg2"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10"
                             class="rev-slidebg" 
                             data-no-retina="">
                    </li>
                    
                    <!-- SLIDE NR. 3 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/corporativo/Slider03.jpg"  
                             alt="Image" 
                             title="slider_bg3"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" 
                             class="rev-slidebg"
                             data-no-retina="">
                       
                    </li>

                    <!-- SLIDE NR. 4 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/corporativo/Slider04.jpg"  
                             alt="Image" 
                             title="slider_bg4"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" 
                             class="rev-slidebg"
                             data-no-retina="">
                       
                    </li>

                    <!-- SLIDE NR. 5 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/corporativo/Slider05.jpg"  
                             alt="Image" 
                             title="slider_bg5"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" 
                             class="rev-slidebg"
                             data-no-retina="">
                       
                    </li>
                    
                </ul>
            </div>
        
             @include('layouts.bookingForm')

            <!-- ========== PLACER ATENDERLES ========== -->
            <section id="">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            <div id="rooms">
                                <div class="main_title mt_wave a_center">
                                    <h2 class="upper">{{ trans('main.placer')}}</h2>
                                </div> 
                                    <h5 class="main_description a_center">{{trans('main.bienvenidoconcepto')}}</h5>
                                <div class="row" id="destinos">

                                    <div class="col-md-3">
                                        <article class="room">
                                            <figure>
                                                <figcaption>
                                                    <h5><a href="/queretaro">Querétaro</a></h5>
                                                </figcaption>
                                                <a class="hover_effect h_yellow h_link" href="/queretaro">
                                                    <img src="hotel/images/corporativo/queretaro.jpg" class="img-responsive" alt="Image">
                                                </a>
                                                <figcaption>
                                                    <h5><a class="button btn_blue" href="/queretaro">Más Información</a></h5>
                                                </figcaption>
                                            </figure>
                                        </article>
                                    </div>

                                    <div class="col-md-3">
                                        <article class="room">
                                            <figure>
                                                <figcaption>
                                                    <h5><a href="/irapuato">Irapuato</a></h5>
                                                </figcaption>
                                                <a class="hover_effect h_yellow h_link" href="/irapuato">
                                                    <img src="hotel/images/corporativo/irapuato.jpg" class="img-responsive" alt="Image">
                                                </a>
                                                <figcaption>
                                                    <h5><a class="button btn_blue" href="/irapuato">Más Información</a></h5>
                                                </figcaption>
                                            </figure>
                                        </article>
                                    </div>

                                    <div class="col-md-3">
                                        <article class="room">
                                            <figure>
                                                <figcaption>
                                                    <h5><a href="/galerias">Celaya Galerías</a></h5>
                                                </figcaption>
                                                <a class="hover_effect h_yellow h_link" href="/galerias">
                                                    <img src="hotel/images/corporativo/galerias.jpg" class="img-responsive" alt="Image">
                                                </a>
                                                <figcaption>
                                                    <h5><a class="button btn_blue" href="/galerias">Más Información</a></h5>
                                                </figcaption>
                                            </figure>
                                        </article>
                                    </div>

                                    <div class="col-md-3">
                                        <article class="room">
                                            <figure>
                                                <figcaption>
                                                    <h5><a href="/veleros">Celaya Veleros</a></h5>
                                                </figcaption>
                                                <a class="hover_effect h_yellow h_link" href="/veleros">
                                                    <img src="hotel/images/corporativo/veleros.jpg" class="img-responsive" alt="Image">
                                                </a>
                                                <figcaption>
                                                    <h5><a class="button btn_blue" href="/veleros">Más Información</a></h5>
                                                </figcaption>
                                            </figure>
                                        </article>
                                    </div>

                                </div>
                            </div>
                        </div> 
                        
                    </div>
                </div>
            </section> 

@endsection

@section('javascripts')
<!-- ========== REVOLUTION SLIDER ========== -->
    <script type="text/javascript" src="hotel/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.video.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("home");
        d.className += " active";
    });
</script>
@endsection
