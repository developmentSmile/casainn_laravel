<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

            <!-- ========== SEO ========== -->
            <title>Casa Inn - </title>
            <meta content="Smile Marketing Digital" name="author">
            <meta content="" name="description">
            <meta content="" name="keywords">

            <!-- ========== FAVICON ========== -->
            <link rel="apple-touch-icon-precomposed" href="hotel/images/favicon-apple.png" />
            <link rel="icon" href="hotel/images/favicon.png">

            <!-- ========== FONTS ========== -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
            <link href="hotel/fonts/flaticon.css" rel="stylesheet">

            <!-- ========== STYLES ========== -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
            {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}


            @yield('styles')
        </head>

        <body id="app-layout">

            @yield('content')



            <!-- ========== JAVASCRIPT ========== -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
            {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

            @yield('javascripts')

        </body>

</html>
