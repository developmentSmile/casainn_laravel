<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

            <!-- ========== SEO ========== -->
            <title>Casa Inn</title>
            <meta content="Smile Marketing Digital" name="author">
            <meta content="" name="description">
            <meta content="" name="keywords">

            <!-- ========== FAVICON ========== -->
            <link rel="apple-touch-icon-precomposed" href="hotel/images/favicon-apple.png" />
            <link rel="icon" href="hotel/images/favicon.png">

            <!-- ========== FONTS ========== -->
            <link rel="stylesheet" href="hotel/css/font-awesome.min.css" type="text/css">
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
            <link href="hotel/fonts/flaticon.css" rel="stylesheet">

            <!-- ========== STYLES ========== -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
            {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

            <!--<link href="hotel/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> -->
            <link href="hotel/css/animate.min.css" rel="stylesheet" type="text/css">
            <link href="hotel/css/famfamfam-flags.min.css" rel="stylesheet" type="text/css">
            <link href="hotel/css/magnific-popup.min.css" rel="stylesheet" type="text/css">
            <link href="hotel/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
            <link href="hotel/css/style.css" rel="stylesheet" type="text/css">
            <link href="hotel/css/responsive.css" rel="stylesheet" type="text/css">

            @yield('styles')
        </head>

        <body id="app-layout">
            <!-- ========== PRELOADER ========== -->
            <div id="loading">
                <div class="inner">
                    <!--Gif Loader-->
                    <div class="loading_effect">
                        <img src="hotel/images/loader.gif" class="loader-gif">
                    </div>
                </div>
            </div>
            
            <div class="wrapper">
                
                <!-- ========== TOP MENU ========== -->
                <div class="top_menu">
                    <div class="container">
                        <ul class="top_menu_right">
                            <li><i class="fa fa-phone"></i><a href="tel:018002000400"> 01 800 200 0400 </a></li>
                            <li class="language-switcher">
                                <nav class="dropdown">
                                    <a href="#" class="dropdown-toggle select" data-hover="dropdown" data-toggle="dropdown">
                                        <i class="famfamfam-flag-mx "></i>Español<b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"><i class="famfamfam-flag-us "></i>English</a></li>
                                    </ul>
                                </nav>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- ========== HEADER ========== -->
                <header>
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle mobile_menu_btn" data-toggle="collapse" data-target=".mobile_menu" aria-expanded="false">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/">
                                <img src="hotel/images/logo.png" alt="Logo">
                            </a>
                        </div>
                        <nav id="main_menu" class="mobile_menu navbar-collapse">
                            <ul class="nav navbar-nav" id="mainmenu">
                                <li class="mobile_menu_title" style="display:none;">MENU</li>
                                <li><a href="/" id="home">{{ trans('menu.bienvenido') }}</a></li>

                                <li class="dropdown simple_menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ trans('menu.destinos') }} <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/queretaro">QUERÉTARO</a></li>
                                        <li><a href="/irapuato">IRAPUATO</a></li>
                                        <li><a href="/galerias">CELAYA GALERÍAS</a></li>
                                        <li><a href="/veleros">CELAYA VELEROS</a></li>
                                    </ul>
                                </li>

                                
                                <li><a href="/contact" id="contact">{{ trans('menu.contactanos') }}</a></li>
                                <li class="menu_button">
                                    <a href="#" class="button btn_blue"><i class="fa fa-calendar"></i>{{ trans('menu.reserva') }}</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </header>

                @yield('content')
                @include('layouts.footer')
     </div>

            <!-- ========== BACK TO TOP ========== -->
            <div id="back_to_top">
                <i class="fa fa-angle-up" aria-hidden="true"></i>
            </div>

            <!-- ========== JAVASCRIPT ========== -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
            {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

            <script type="text/javascript" src="hotel/js/jquery.min.js"></script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_gU3RtvZYHP3N5_wyYrRHf_MzWFFAsiU"></script>
            <script type="text/javascript" src="hotel/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="hotel/js/bootstrap-datepicker.min.js"></script>
            <!--<script type="text/javascript" src="hotel/js/bootstrap-select.min.js"></script>-->
            <script type="text/javascript" src="hotel/js/jquery.smoothState.min.js"></script>
            <script type="text/javascript" src="hotel/js/moment.min.js"></script>
            <script type="text/javascript" src="hotel/js/morphext.min.js"></script>
            <script type="text/javascript" src="hotel/js/wow.min.js"></script>
            <script type="text/javascript" src="hotel/js/jquery.easing.min.js"></script>
            <script type="text/javascript" src="hotel/js/owl.carousel.min.js"></script>
            <script type="text/javascript" src="hotel/js/owl.carousel.thumbs.min.js"></script>
            <script type="text/javascript" src="hotel/js/jquery.magnific-popup.min.js"></script>
            <script type="text/javascript" src="hotel/js/jPushMenu.min.js"></script>
            <script type="text/javascript" src="hotel/js/isotope.pkgd.min.js"></script>
            <script type="text/javascript" src="hotel/js/countUp.min.js"></script>
            <script type="text/javascript" src="hotel/js/jquery.countdown.min.js"></script>
            <script type="text/javascript" src="hotel/js/main.js"></script>
            @yield('javascripts')

        </body>

</html>
