    <!-- ========== FOOTER ========== -->
        <footer>
            <div class="inner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 widget">
                            <div class="about">
                                <a href="index.html"><img class="logo" src="hotel/images/logo.png" height="32" alt="Logo"></a>
                                <h5>{{ trans('footer.textoservclientes') }}</h5>
                                <ul class="blog_posts">
                                    <li><a href="/#destinos">{{ trans('footer.destinos') }}</a></li>
                                    <li><a href="/">{{ trans('footer.facturacion') }}</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 widget">
                            <h5>{{ trans('footer.titulopoliticas') }}</h5>
                            <ul class="blog_posts">
                                <li><a href="/privacy">{{ trans('footer.textopoliticas') }}</a></li>
                            </ul>
                        </div>

                        <div class="col-md-3 col-sm-6 widget">
                            <h5>{{ trans('footer.titulotelefonos') }}</h5>
                            <ul class="blog_posts">
                                <li><a href="/queretaro">QUERÉTARO</a><small>+52 (442) 101 4100</small></li>
                                <li><a href="/veleros">CELAYA VELEROS</a><small>+52 (461) 598 6700</small></li>
                                <li><a href="/galerias">CELAYA GALERIAS</a><small>+52 (461) 229 6250</small></li>
                                <li><a href="/irapuato">IRAPUATO</a><small>+52 (462) 119 3200</small></li>
                            </ul>
                        </div>

                        <div class="col-md-3 col-sm-6 widget">
                            <h5>{{ trans('footer.tituloreserv') }}</h5>
                            <address>
                                <ul class="address_details">
                                    <li><i class="glyphicon glyphicon-phone-alt"></i> 01 800 200 0400</li>
                                </ul>
                            </address>
                            <div class="social_media">
                                <a class="facebook" data-original-title="Facebook" data-toggle="tooltip" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="youtube" data-original-title="Youtube" data-toggle="tooltip" href="#"><i class="fa fa-youtube-play"></i></a>
                                <a class="twitter" data-original-title="Twitter" data-toggle="tooltip" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="googleplus" data-original-title="Google Plus" data-toggle="tooltip" href="#"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </footer>