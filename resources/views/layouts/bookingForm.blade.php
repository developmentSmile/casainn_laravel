             <!-- ========== HORIZONTAL BOOKING FORM ========== -->
            <div class="hbf">
                <div class="container">
                    <div class="inner">

                        <form id="booking-form" action="#" method="get">
                            <div class="col-md-2 md_p5">
                                <div class="form-group">
                                    <label>Hotel</label>
                                    <div class="form_select">
                                        <div class="btn-group bootstrap-select form-control">
                                            <select name="PropertyNumber" id="PropertyNumber" class="" title="Selecciona un hotel" required>
                                                <option class="bs-title-option" value="">Selecciona un hotel</option>
                                                <option value="6486">Casa Inn Querétaro</option>
                                                <option value="6487">Casa Inn Galerias</option>
                                                <option value="6488">Casa Inn Irapuato</option>
                                                <option value="6489">Casa Inn Casa Inn Celaya</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="row">

                                    <div class="col-md-6 col-sm-6 arrival_date md_pl5 md_nopadding_right">
                                        <div class="form-group">
                                            <label>Fecha de Llegada</label>
                                            <div class="form_date">
                                                <input type="text" id="checkin" class="datepicker form-control md_noborder_right" name="CheckIn" placeholder="Fecha de Llegada" required>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 departure_date md_pr5 md_nopadding_left">
                                        <div class="form-group">
                                            <label>Fecha de Salida</label>
                                            <div class="form_date departure">
                                                <input type="text" id="checkout" class="datepicker form-control" name="CheckOut" placeholder="Fecha de Salida" required>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-2 md_p5">
                                <div class="form-group">
                                    <label>Habitaciones</label>
                                    <div class="form_select">
                                        <div class="btn-group bootstrap-select form-control">                   
                                            <select name="Rooms" class="" title="Selecciona una habitación" required>
                                                <option class="bs-title-option" value="">Numero de Habitaciones</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>                        
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1 md_p5">
                                <div class="form-group">
                                    <label>Adultos</label>
                                    <div class="form_select">
                                        <div class="btn-group bootstrap-select form-control">                   
                                            <select name="Adults" class="" title="Selecciona un adulto" required>
                                                <option class="bs-title-option" value="0">Adultos</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>                        
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-1 md_p5">
                                <div class="form-group">
                                    <label>Menores</label>
                                    <div class="form_select">
                                        <div class="btn-group bootstrap-select form-control">                   
                                            <select name="Children" class="" title="Selecciona un menor" required>
                                                <option class="bs-title-option" value="0">Menores</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>                        
                                                <option value="4">4</option>
                                                <option value="5">5</option>                        
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 md_pl5">
                                <button type="button" class="button btn_blue btn_full upper sendbooking">
                                    Buscar
                                </button>
                            </div>
                            
                            <input type="hidden" name="Provider" value="0" />
                            <input type="hidden" name="AccessCode" value="" />
                            <input type="hidden" name="NegociateRate" value="" />
                            <input type="hidden" name="Currency" value="MXN" />
                            <input type="hidden" name="Tab" value="Rates" />
                                
                        </form>

                    </div>
                </div>
            </div>