@extends('layouts.appqro')

@section('content')
    <!-- =========== GOOGLE MAP ========== -->
        <div id="map">
            <div id="google-map">
            </div>
            <div id="map-canvas"></div>
        </div>

        <!-- ========== MAIN ========== -->
        <main id="contact_page">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-8">
                        <div class="main_title a_left">
                            <h2>{{ trans('queretaro.contactanos')}}</h2>
                        </div>
                        <form id="contact-form-page">
                            <div class="row">
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('queretaro.nombre')}}</label>
                                    <input type="text" class="form-control" name="name" placeholder="{{ trans('queretaro.nombre')}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('queretaro.telefono')}}</label>
                                    <input type="text" class="form-control" name="phone" placeholder="{{ trans('queretaro.telefono')}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('queretaro.correo')}}</label>
                                    <input type="email" class="form-control" name="email" placeholder="{{ trans('queretaro.correo')}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('queretaro.asunto')}}</label>
                                    <input type="text" class="form-control" name="subject" placeholder="{{ trans('queretaro.asunto')}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">{{ trans('queretaro.mensaje')}}</label>
                                    <textarea class="form-control" name="message" placeholder="{{ trans('queretaro.mensaje')}}"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <button type="submit" class="button  btn_blue mt40 upper pull-right">
                                        <i class="fa fa-paper-plane-o" aria-hidden="true"></i> {{ trans('queretaro.enviar')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="main_title a_left">
                            <h2>{{ trans('queretaro.datos')}}</h2>
                        </div>
                        <ul class="contact-info">
                            <li>
                                <span>{{ trans('queretaro.direccion')}}:</span> Paseo de Miranda Ote 2,<br>Fracc Monte Miranda Querétaro<br>Querétaro, México, C.P 76240
                            </li>
                            <li>
                                <span>{{ trans('queretaro.correo')}}:</span> reservaqro@casainn.com.mx
                            </li>
                            <li>
                                <span>{{ trans('queretaro.telefono')}}:</span> +52 <strong>(442) 101 41 00</strong>
                            </li>
                        </ul>
                        <div class="social_media sm_contact">
                            <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                            <a class="youtube" href="#"><i class="fa fa-youtube"></i></a>
                            <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                            <a class="googleplus" href="#"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </main>

@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("contact");
        d.className += " active";
    });
</script>

<script type="text/javascript">
/*========== GOOGLE MAP ==========*/
        function initialize() {
            var map;
            var panorama;
            //20.586924, -100.347490 qro
            var var_latitude = 20.586924; // Google Map Latitude
            var var_longitude = -100.347490; // Google Map Longitude
            var pin = '../hotel/images/icons/pin.png';

            //Map pin-window details
            var title = "Casa inn - Querétaro";
            var hotel_name = "Casa inn - Querétaro";
            var hotel_address = "Paseo de Miranda Ote 2,Fracc Monte Miranda Querétaro";
            var hotel_desc = "Hotel";

            var hotel_location = new google.maps.LatLng(var_latitude, var_longitude);
            var mapOptions = {
                center: hotel_location,
                zoom: 15,
                scrollwheel: false, 
                streetViewControl: false
            };
            map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
            var contentString =
                '<div id="infowindow_content">' +
                '<p><strong>' + hotel_name + '</strong><br>' +
                hotel_address + '<br>' +
                hotel_desc + '</p>' +
                '</div>';

            var var_infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            var marker = new google.maps.Marker({
                position: hotel_location,
                map: map,
                icon: pin,
                title: title,
                maxWidth: 500,
                optimized: false,
            });
            google.maps.event.addListener(marker, 'click', function () {
                var_infowindow.open(map, marker);
            });
        }

        //Check if google map div exist
        if ($("#map-canvas").length > 0){
           google.maps.event.addDomListener(window, 'load', initialize);
        }
</script>
@endsection