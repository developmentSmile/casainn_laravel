@extends('layouts.appqro')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('queretaro.estandarmat')}}</h3>
        </div>

    <!-- =========== MAIN ========== -->
        <main id="room_page">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="slider">
                            <div id="slider-larg" class="owl-carousel owl-demo">
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/deluxe01.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/deluxe02.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/deluxe03.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/deluxe04.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/deluxe05.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/deluxe06.jpg" alt="Image">
                                </div>

                            </div>
                            <div id="thumbs" class="owl-carousel">
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/deluxe001.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/deluxe002.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/deluxe003.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/deluxe004.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/deluxe005.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/deluxe006.jpg" alt="Image"></div>
                                
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">

                        <div class="t_style a_left s_title mt50">
                            <p class="text-center">{{ trans('queretaro.textoestandarmat')}}</p>
                            <div class="c_inner mt50">
                                <h2 class="c_title">{{ trans('queretaro.serviciosastandarmat')}}</h2>
                            </div>
                        </div>

                        <div class="room_facilitys_list">
                            <div class="all_facility_list">
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-phone"></i>{{ trans('queretaro.2tel')}}</li>
                                        <li><i class="fa fa-building"></i>{{ trans('queretaro.32mt2')}}</li>
                                        <li><i class="fa fa-snowflake-o"></i>{{ trans('queretaro.aire')}}</li>
                                        <li><i class="fa fa-coffee"></i>{{ trans('queretaro.cafetera')}}</li>
                                        <li><i class="fa fa-lock"></i>{{ trans('queretaro.seguridad')}}</li>
                                        <li><i class="fa fa-key"></i>{{ trans('queretaro.chapas')}}</li>
                                        <li><i class="fa fa-briefcase"></i>{{trans('queretaro.escritorio')}}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-cutlery"></i>{{ trans('queretaro.frigobar')}}</li>
                                        <li><i class="fa fa-shower"></i>{{ trans('queretaro.kitamenidades')}}</li>
                                        <li><i class="fa fa-black-tie"></i>{{ trans('queretaro.kitplanchado')}}</li>
                                        <li><i class="fa fa-lightbulb-o"></i>{{ trans('queretaro.lamparas')}}</li>
                                        <li><i class="fa fa-television"></i>{{ trans('queretaro.pantallaled')}}</li>
                                        <li id="planos"><i class="fa fa-map"></i>{{ trans('queretaro.planos') }}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-clock-o"></i>{{trans('queretaro.despertador')}}</li>
                                        <li><i class="fa fa-volume-up"></i>{{ trans('queretaro.sonido')}}</li>
                                        <li><i class="fa fa-fire-extinguisher"></i>{{ trans('queretaro.incendios')}}</li>
                                        <li><i class="fa fa-wifi"></i>{{ trans('queretaro.wifi') }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div id="myModal" class="modal">
                            <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
                            <img class="modal-content" id="img01">
                            <div id="caption"></div>
                        </div>

                        <div class="col-md-4 col-md-offset-4 mt40">
                            <a href="/queretaro" class="button  btn_blue btn_full upper">{{ trans('menu.reserva') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>

@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("rooms");
        d.className += " active";

        /*
        JS Modal
         */
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var text = document.getElementById('planos');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        text.onclick = function(){
            modal.style.display = "block";
            modalImg.src = "hotel/images/queretaro/Queretaro_Cuarto_Tipo.jpg";//Nombre Archivo
            captionText.innerHTML = "Deluxe con 2 camas matrimoniales"; //Descripcion imagen
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    });
</script>
@endsection