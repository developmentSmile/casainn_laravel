@extends('layouts.appqro')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('menu.salones')}}</h3>
        </div>

    <!-- ========== MAIN ========== -->
        <main id="events_page">
            <div class="container">
                

                <!-- ITEM SALON QRO-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 500</div>
                                <div class="month"> {{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('queretaro.salonqro')}}</h5>
                                <div class="meta">
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('queretaro.dessalonqro')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.montajesalonqro')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/queretaro/Gran_Salon_Queretaro.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM QRO 1 Y 2-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 250</div>
                                <div class="month"> {{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>QUERÉTARO 1, 2 y 3</h5>
                                <div class="meta">
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('queretaro.qrodes')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.montajeqro')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/queretaro/MARQUES23.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM El marques-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 200</div>
                                <div class="month"> {{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('queretaro.marques')}}</h5>
                                <div class="meta">
                                    <div class="time">160 {{ trans('queretaro.m2')}}</div>
                                    <div class="location">3.35 {{ trans('queretaro.metrosaltura')}}</div>
                                </div>
                                <div class="description">
                                    <p>{{ trans('queretaro.marquezdes')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.divisible')}}</li>
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.montajemarques')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/queretaro/Salon_Marques.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM Marques 12-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 80</div>
                                <div class="month"> {{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('queretaro.salonmarques')}}</h5>
                                <div class="meta">
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('queretaro.salonmarquesdes')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.salonmarquesmontaje')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/queretaro/MARQUES12.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM sky-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 40</div>
                                <div class="month"> {{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('queretaro.salonmarques23')}}</h5>
                                <div class="meta">
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('queretaro.salonmarques23des')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.salonmarques23montaje')}}</li>
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.mesafijaimperial')}}</li>
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.vga')}}</li>
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.pantallasalon')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/queretaro/QRO12.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- ITEM -->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 10 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('queretaro.gobernador')}}</h5>
                                <div class="meta"></div>
                                <div class="description">
                                    <p>{{ trans('queretaro.gobernadordes')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.montajegob')}}</li>
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.sillaseg')}}</li>
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.pantalla42')}}</li>
                                        <li><i class="fa fa-check"></i> {{ trans('queretaro.conexion')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/queretaro/Salon_Gobernador.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-4 mt40">
                    <a href="/contact-queretaro" class="button  btn_blue btn_full upper">{{ trans('menu.cotiza') }}</a>
                </div>
            </div>
        </main>

@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("salon");
        d.className += " active";
    });
</script>
@endsection