@extends('layouts.appqro')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('queretaro.master')}}</h3>
        </div>
    <!-- =========== MAIN ========== -->
        <main id="room_page">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="slider">
                            <div id="slider-larg" class="owl-carousel">
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/master06.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/master01.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/master02.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/master03.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/master04.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/master05.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/queretaro/master07.jpg" alt="Image">
                                </div>
                            </div>
                            <div id="thumbs" class="owl-carousel">
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/master006.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/master001.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/master002.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/master003.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/master004.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/master005.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/queretaro/master007.jpg" alt="Image"></div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">

                        <div class="main_title mt50"></div>
                        <p class="text-center">{{ trans('queretaro.textomaster')}}</p>
                        
                        <div class="t_style a_left s_title mt50">
                            <div class="c_inner">
                                <h2 class="c_title">{{ trans('queretaro.serviciomaster')}}</h2>
                            </div>
                        </div>
                        <div class="room_facilitys_list">
                            <div class="all_facility_list">
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-shower"></i>{{ trans('queretaro.baño')}}</li>
                                        <li><i class="fa fa-television"></i>2 {{ trans('queretaro.pantallasled')}}</li>
                                        <li><i class="fa fa-phone"></i>{{ trans('queretaro.2tel')}}</li>
                                        <li><i class="fa fa-building"></i>{{ trans('queretaro.67mt2')}}</li>
                                        <li><i class="fa fa-snowflake-o"></i>{{ trans('queretaro.aire')}}</li>
                                        <li><i class="fa fa-coffee"></i>{{ trans('queretaro.cafetera')}}</li>
                                        <li><i class="fa fa-lock"></i>{{ trans('queretaro.seguridad')}}</li>
                                        <li><i class="fa fa-key"></i>{{ trans('queretaro.chapas')}}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-cutlery"></i>{{ trans('queretaro.cocineta')}}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('queretaro.comedor')}}</li>
                                        <li><i class="fa fa-briefcase"></i>{{ trans('queretaro.escritorio')}}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('queretaro.frigobar')}}</li>
                                        <li><i class="fa fa-shower"></i>{{ trans('queretaro.kitamenidades')}}</li>
                                        <li><i class="fa fa-lightbulb-o"></i>{{ trans('queretaro.lamparas')}}</li>
                                        <li id="planos"><i class="fa fa-map"></i>{{ trans('queretaro.planos') }}</li>
                                        <li><i class="fa fa-clock-o"></i>{{ trans('queretaro.despertador')}}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding_left">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-book"></i>{{ trans('queretaro.sala')}}</li>
                                        <li><i class="fa fa-plug"></i>{{ trans('queretaro.secadora')}}</li>
                                        <li><i class="fa fa-volume-up"></i>{{ trans('queretaro.sonido')}}</li>
                                        <li><i class="fa fa-fire-extinguisher"></i>{{ trans('queretaro.incendios')}}</li>
                                        <li><i class="fa fa-wifi"></i>{{ trans('queretaro.wifi') }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div id="myModal" class="modal">
                            <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
                            <img class="modal-content" id="img01">
                            <div id="caption"></div>
                        </div>

                        <div class="col-md-4 col-md-offset-4 mt40">
                            <a href="/queretaro" class="button  btn_blue btn_full upper">{{ trans('menu.reserva') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>


@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("rooms");
        d.className += " active";

        /*
         JS Modal
         */
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var text = document.getElementById('planos');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        text.onclick = function(){
            modal.style.display = "block";
            modalImg.src = "hotel/images/queretaro/Queretaro_Master_Suite.jpg";//Nombre Archivo
            captionText.innerHTML = "Master Suite"; //Descripcion imagen
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    });
</script>
@endsection