@extends('layouts.appqro')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('menu.habanner')}}</h3>
        </div>

    <!-- =========== MAIN ========== -->
        <main id="rooms_list">
           <div class="container">
           
              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/deluxeking-queretaro" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/queretaro/deluxek01.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/deluxeking-queretaro"> {{ trans('queretaro.estandarks')}} </a></h3>
                             <p class="text-justify">{{ trans('queretaro.desestandarks')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i> 
                                <i class="fa fa-cutlery"></i>  
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/queretaro" class="button  btn_blue btn_full upper">{{ trans('queretaro.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>

              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/deluxematrimonial-queretaro" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/queretaro/deluxe_Doble.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/deluxematrimonial-queretaro"> {{ trans('queretaro.estandarmat')}} </a></h3>
                             <p class="text-justify">{{ trans('queretaro.desestandarmat')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i> 
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i>
                                <i class="fa fa-cutlery"></i> 
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/queretaro" class="button  btn_blue btn_full upper">{{ trans('queretaro.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>
              
              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/suites-queretaro" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/queretaro/Suite.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/suites-queretaro"> {{ trans('queretaro.suites')}} </a></h3>
                             <p class="text-justify">{{ trans('queretaro.dessuites')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i> 
                                <i class="fa fa-cutlery"></i>
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/queretaro" class="button  btn_blue btn_full upper">{{ trans('queretaro.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>

              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/master-queretaro" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/queretaro/Master_Suite.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/master-queretaro"> {{ trans('queretaro.master')}} </a></h3>
                             <p class="text-justify">{{ trans('queretaro.desmaster')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i> 
                                <i class="fa fa-cutlery"></i>
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/queretaro" class="button  btn_blue btn_full upper">{{ trans('queretaro.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>

              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/handicap-queretaro" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/queretaro/deluxe_Doble.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/handicap-queretaro"> {{ trans('queretaro.handicap')}} </a></h3>
                             <p class="text-justify">{{ trans('queretaro.textohan')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i> 
                                <i class="fa fa-cutlery"></i>
                                <i class="fa fa-wheelchair"></i>
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/queretaro" class="button  btn_blue btn_full upper">{{ trans('queretaro.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>

           </div>
        </main>


@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("rooms");
        d.className += " active";
    });
</script>
@endsection