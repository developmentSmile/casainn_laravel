@extends('layouts.appgalerias')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title2">
            <img src="hotel/images/galerias/BANNER_SALONES.jpg" class="img-responsive">
        </div>

    <!-- ========== MAIN ========== -->
        <main id="events_page">
            <div class="container">
                
                <!-- ITEM BAJIO-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date">30</div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('galerias.bajio')}}</h5>
                                <div class="meta">
                                  <div class="time">34.8 {{ trans('queretaro.m2')}}</div>
                                </div>
                                <div class="description">
                                    <p>{{ trans('galerias.bajiodes')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('galerias.bajiomontaje')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/galerias/salones.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM BAJIO 12-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date">70</div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('galerias.bajio12')}}</h5>
                                <div class="meta">
                                  <div class="time">69.6 {{ trans('queretaro.m2')}}</div>
                                </div>
                                <div class="description">
                                    <p>{{ trans('galerias.bajio12des')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('galerias.bajio12montaje')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/galerias/salones.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM BAJIO 5-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date">60</div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('galerias.bajio5')}}</h5>
                                <div class="meta">
                                  <div class="time">53.2 {{ trans('queretaro.m2')}}</div>
                                </div>
                                <div class="description">
                                    <p>{{ trans('galerias.bajio5des')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('galerias.bajio5montaje')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/galerias/salones.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM Sala-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date">12</div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('galerias.sala')}}</h5>
                                <div class="meta">
                                  <div class="time">32.2 {{ trans('queretaro.m2')}}</div>
                                </div>
                                <div class="description">
                                    <p>{{ trans('galerias.salades')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('galerias.salamontaje')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/galerias/sala.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-4 col-md-offset-4 mt40">
                    <a href="/contact-galerias" class="button  btn_blue btn_full upper">{{ trans('menu.cotiza') }}</a>
                </div>
            </div>
        </main>

@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("salon");
        d.className += " active";
    });
</script>
@endsection