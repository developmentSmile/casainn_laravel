@extends('layouts.appgalerias')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('menu.habanner')}}</h3>
        </div>

    <!-- =========== MAIN ========== -->
        <main id="rooms_list">
           <div class="container">

              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/deluxeking-galerias" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/galerias/estandar.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/deluxeking-galerias"> {{ trans('galerias.estandar')}} </a></h3>
                             <p class="text-justify">{{ trans('galerias.desestandar')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i> 
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i>
                                <i class="fa fa-cutlery"></i> 
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/galerias" class="button  btn_blue btn_full upper">{{ trans('galerias.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>

              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/deluxematrimonial-galerias" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/galerias/doble.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/deluxematrimonial-galerias"> {{ trans('galerias.doble')}} </a></h3>
                             <p class="text-justify">{{ trans('galerias.desdoble')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i> 
                                <i class="fa fa-cutlery"></i>  
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/galerias" class="button  btn_blue btn_full upper">{{ trans('galerias.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>

              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/suites-galerias" class="hover_effect h_link h_yellow">
                           <img src="hotel/images/galerias/suites.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/suites-galerias"> {{ trans('galerias.suites')}} </a></h3>
                             <p class="text-justify">{{ trans('galerias.dessuites')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i> 
                                <i class="fa fa-cutlery"></i>
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/galerias" class="button  btn_blue btn_full upper">{{ trans('galerias.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>
              
           </div>
        </main>


@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("rooms");
        d.className += " active";
    });
</script>
@endsection