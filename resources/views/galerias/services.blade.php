@extends('layouts.appgalerias')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('menu.servicios')}}</h3>
        </div>

    <!-- =========== MAIN ========== -->
        <main id="room_page">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="slider">
                            <div id="slider-larg" class="owl-carousel">
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/servicios001.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/servicios002.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/servicios003.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/servicios004.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/servicios005.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/servicios006.jpg" alt="Image">
                                </div>
                            </div>
                            <div id="thumbs" class="owl-carousel">
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/servicios01.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/servicios02.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/servicios03.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/servicios04.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/servicios05.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/servicios06.jpg" alt="Image"></div>
                            </div>
                        </div>

                        </div>
                    <div class="col-md-12">

                        <div class="main_title t_style a_left s_title mt50">
                            <div class="c_inner">
                                <h3 class="c_title">{{ trans('galerias.titulohotelcuenta')}}</h3>
                            </div>
                        </div>

                        <div class="room_facilitys_list">
                            <div class="all_facility_list">
                                <div class="col-sm-5 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i>{{ trans('services.alberca')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.computo')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.computadoras')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.lavado')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.bar')}}*</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.caja')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.camaras')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.nado')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.canal')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.cenas')}}*</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.chapoteadero')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.desayunoamericano')}}</li>

                                        <li>{{ trans('services.costoextra')}}</li>
                                        
                                    </ul>

                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i>{{ trans('services.estacionamiento')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.discapacidad')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.gym')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.discapacitados')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.internet')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.personal')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.roomservice')}}*</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.cunas')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.lavanderia')}}*</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.incendio')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.transporte')}}</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.vitrina')}}*</li>
                                        <li><i class="fa fa-check"></i>{{ trans('services.wifi')}}</li>
                                        
                                    </ul>

                                    <img src="hotel/images/galerias/gym.jpg" class="img-responsive">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-4 mt40">
                            <a href="/galerias" class="button  btn_blue btn_full upper">{{ trans('menu.reserva') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>

@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("services");
        d.className += " active";
    });
</script>
@endsection