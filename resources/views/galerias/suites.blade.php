@extends('layouts.appgalerias')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('galerias.suites')}}</h3>
        </div>

    <!-- =========== MAIN ========== -->
        <main id="room_page">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="slider">
                            <div id="slider-larg" class="owl-carousel owl-demo">
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/suite001.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/suite002.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/suite003.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/suite004.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/suite005.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/galerias/suite006.jpg" alt="Image">
                                </div>
                            </div>
                            <div id="thumbs" class="owl-carousel">
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/suite01.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/suite02.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/suite03.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/suite04.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/suite05.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/galerias/suite06.jpg" alt="Image"></div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        
                        <div class="t_style a_left s_title mt50">
                            <p class="text-center">{{ trans('galerias.textosuites')}}</p>
                            <div class="c_inner mt50">
                                <h2 class="c_title">{{ trans('galerias.serviciossuites')}}</h2>
                            </div>
                        </div>

                        <div class="room_facilitys_list">
                            <div class="all_facility_list">
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                       <li><i class="fa fa-television"></i>2 {{ trans('galerias.smart')}}</li>
                                        <li><i class="fa fa-snowflake-o"></i>{{ trans('galerias.aire')}}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('galerias.barra')}}</li>
                                        <li><i class="fa fa-coffee"></i>{{ trans('galerias.cafe')}}</li>
                                        <li><i class="fa fa-key"></i>{{ trans('galerias.chapas')}}</li>
                                        <li><i class="fa fa-lock"></i>{{ trans('galerias.seguridad')}}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('galerias.utensilios')}}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('galerias.cocineta')}}</li>
                                        <li><i class="fa fa-briefcase"></i>{{ trans('galerias.escritorio')}}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('galerias.frigobar')}}</li> 
                                        
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-bed"></i>{{ trans('galerias.futon')}}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('galerias.hielera')}}</li>
                                        <li><i class="fa fa-bath"></i>{{ trans('galerias.baño')}}</li>
                                        <li><i class="fa fa-black-tie"></i>{{ trans('galerias.planchado')}}</li>
                                        <li><i class="fa fa-lightbulb-o"></i>{{ trans('galerias.lamparas')}}</li>
                                        <li><i class="fa fa-television"></i>{{ trans('galerias.canales')}}</li>
                                        <li><i class="fa fa-bed"></i>{{ trans('galerias.almohadas')}}</li>
                                        <li id="planos"><i class="fa fa-map"></i>{{ trans('queretaro.planos') }}</li>
                                        <li><i class="fa fa-lock"></i>{{ trans('galerias.puertas')}}</li>
                                        <li><i class="fa fa-cutlery"></i>{{ trans('galerias.refri')}}</li>
                                        
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-shower"></i>{{ trans('galerias.regadera')}}</li>
                                        <li><i class="fa fa-clock-o"></i>{{ trans('galerias.despertador')}}</li>
                                        <li><i class="fa fa-plug"></i>{{trans('galerias.secadora')}}</li>
                                        <li><i class="fa fa-fire-extinguisher"></i>{{trans('galerias.incendio')}}</li>
                                        <li><i class="fa fa-plug"></i>{{ trans('galerias.energia')}}</li>
                                        <li><i class="fa fa-phone"></i>{{ trans('galerias.telefono')}}</li>
                                        <li><i class="fa fa-bath"></i>{{ trans('galerias.tina')}}</li>
                                        <li><i class="fa fa-window-maximize"></i>{{ trans('galerias.ventanas')}}</li>
                                        <li><i class="fa fa-wifi"></i>{{ trans('galerias.wifi')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div id="myModal" class="modal">
                            <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
                            <img class="modal-content" id="img01">
                            <div id="caption"></div>
                        </div>

                        <div class="col-md-4 col-md-offset-4 mt40">
                            <a href="/galerias" class="button  btn_blue btn_full upper">{{ trans('menu.reserva') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>

@endsection

@section('javascripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var d = document.getElementById("rooms");
            d.className += " active";

            /*
             JS Modal
             */
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var text = document.getElementById('planos');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            text.onclick = function(){
                modal.style.display = "block";
                modalImg.src = "hotel/images/galerias/Galerias_Junior_Suite.jpg";//Nombre Archivo
                captionText.innerHTML = "{{ trans('galerias.suites')}}"; //Descripcion imagen
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
                modal.style.display = "none";
            }
        });
    </script>
@endsection