@extends('layouts.appgalerias')

@section('styles')
    <link href="hotel/revolution/css/layers.css" rel="stylesheet" type="text/css" />
    <link href="hotel/revolution/css/settings.css" rel="stylesheet" type="text/css" />
    <link href="hotel/revolution/css/navigation.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
        <!-- ========== REVOLUTION SLIDER ========== -->
            <div id="classic_slider" class="rev_slider" style="display:none">
                <ul>
                    <!-- SLIDE NR. 1 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/galerias/Slider03_galerias.jpg" 
                             alt="Image" 
                             title="slider_bg2"
                             data-bgposition="center center" 
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat" 
                             data-bgparallax="10" 
                             class="rev-slidebg" 
                             data-no-retina="">
                    </li>
                    
                    <!-- SLIDE NR. 2 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/galerias/Slider01_galerias.jpg" 
                             alt="Image" 
                             title="slider_bg2"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10"
                             class="rev-slidebg" 
                             data-no-retina="">
                
                    </li>
                    
                    <!-- SLIDE NR. 3 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/galerias/Slider05_galerias.jpg"  
                             alt="Image" 
                             title="slider_bg3"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" 
                             class="rev-slidebg"
                             data-no-retina="">
                    </li>

                    <!-- SLIDE NR. 4 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/galerias/Slider04_galerias.jpg"  
                             alt="Image" 
                             title="slider_bg4"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" 
                             class="rev-slidebg"
                             data-no-retina="">
                    </li>

                    <!-- SLIDE NR. 5 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/galerias/Slider02_galerias.jpg"  
                             alt="Image" 
                             title="slider_bg5"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" 
                             class="rev-slidebg"
                             data-no-retina="">
                    </li>
                    
                </ul>
            </div>
        
            @include('layouts.bookingForm')

            <!-- ========== BIENVENIDO  ========== -->
            <section class="white_bg" id="rooms">
                <div class="container">

                    <div class="main_title mt_wave a_center">
                        <h2>{{ trans('galerias.bienvenido')}}</h2>
                    </div>
                    <p class="main_description a_center">{{ trans('galerias.placer')}}</p>

                    <div class="row"> 
                            <div class="col-md-4">
                                <article class="room">
                                    <figure>
                                        <img src="hotel/images/galerias/BIENVENIDOS_01.jpg" class="img-responsive" alt="Image">
                                    </figure>
                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="room">
                                    <figure>
                                        <img src="hotel/images/galerias/BIENVENIDOS_02.jpg" class="img-responsive" alt="Image">
                                    </figure>
                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="room">
                                    <figure>
                                       <img src="hotel/images/galerias/BIENVENIDOS_03.jpg" class="img-responsive" alt="Image">
                                    </figure>
                                </article>
                            </div>
                        </div>
                </div>
            </section>

            <!--IMAGEN-->
            <section id="divisor">
                <div class="col-md-4 col-md-offset-4">
                    <a href="/galerias" class="button  btn_blue btn_full upper">{{ trans('menu.reserva') }}</a>
                </div>
            </section>

            <!-- =========== GALERIA ========== -->
            <section id="gallery">
                <div class="container">
                    <div class="main_title mt_wave a_center">
                        <h2>{{ trans('galerias.titulogaleria')}}</h2>
                    </div>
                    <div class="row">
                        <div class="grid gallery_items">
                            <!-- ITEM 1 -->
                            <figure class="g_item col-md-4 col-sm-6 g_swimming_pool">
                                <img src="./hotel/images/galerias/GALERIA01.jpg" class="img-responsive" alt="Image">
                            </figure>
                            <!-- ITEM 2-->
                            <figure class="g_item col-md-4 col-sm-6  g_swimming_pool">
                                <img src="hotel/images/galerias/GALERIA02.jpg" class="img-responsive" alt="Image">
                            </figure>
                            <!-- ITEM 3-->
                            <figure class="g_item col-md-4 col-sm-6 g_restaurant">
                                <img src="hotel/images/galerias/GALERIA03.jpg" class="img-responsive" alt="Image">
                            </figure>
                            <!-- ITEM 4-->
                            <figure class="g_item col-md-4 col-sm-6 g_restaurant">
                                <img src="hotel/images/galerias/GALERIA04.jpg" class="img-responsive" alt="Image">
                            </figure>
                            <!-- ITEM 5-->
                            <figure class="g_item col-md-4 col-sm-6 g_spa">
                                <img src="hotel/images/galerias/GALERIA05.jpg" class="img-responsive" alt="Image">
                            </figure>
                            <!-- ITEM 6-->
                            <figure class="g_item col-md-4 col-sm-6 g_restaurant">
                                <img src="hotel/images/galerias/GALERIA06.jpg" class="img-responsive" alt="Image">
                            </figure>
                            <!-- ITEM 7-->
                            <figure class="g_item col-md-4 col-sm-6 g_island">
                                <img src="hotel/images/galerias/GALERIA07.jpg" class="img-responsive" alt="Image">
                            </figure>
                            <!-- ITEM 8-->
                            <figure class="g_item col-md-4 col-sm-6 g_island">
                                <img src="hotel/images/galerias/GALERIA08.jpg" class="img-responsive" alt="Image">
                            </figure>
                            <!-- ITEM 9-->
                            <figure class="g_item col-md-4 col-sm-6 g_island">
                                <img src="hotel/images/galerias/GALERIA09.jpg" class="img-responsive" alt="Image">
                            </figure>
                        </div>
                    </div>
                </div>
            </section>


@endsection

@section('javascripts')
<!-- ========== REVOLUTION SLIDER ========== -->
    <script type="text/javascript" src="hotel/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.video.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("home");
        d.className += " active";
    });
</script>
@endsection