@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="entry-content"> 
    <h3 style="text-align: center;">AVISO DE PRIVACIDAD INTEGRAL</h3>
    <p>&nbsp;</p>
    <p style="text-align: justify;">
        <strong>HOTELES CASA INN, S. A. DE C. V.,</strong> siendo su nombre comercial reconocido <strong>CASA INN BUSINESS HOTEL CELAYA</strong>, con domicilio en calle Av. Juan. Jose Torres Landa No.202 Col Fracc. Del Parque C.P. 38010 , en la Ciudad de Celaya, pa&iacute;s M&eacute;xico, y portal de internet <a href="http://www.casainn.com.mx">www.casainn.com.mx</a> es el responsable del uso y protecci&oacute;n de sus datos personales, y al respecto le informamos lo siguiente:
    </p>
    <p style="text-align: justify; margin-left: 40px;">&nbsp;</p>
    <p style="text-align: justify; margin-left: 40px;">
        <strong>&iquest;Para qu&eacute; fines utilizaremos sus datos personales?</strong><br/><br/>&bull; <strong>La informaci&oacute;n personal que nos proporciona ser&aacute; utilizada por HOTELES CASA INN, S. A. DE C. V., primordialmente para</strong> verificar y confirmar su identidad para el hospedaje temporal.</p>
<p style="text-align: justify;">
    De manera adicional, <strong>HOTELES CASA INN, S. A. DE C. V.,</strong> utilizar&aacute; su informaci&oacute;n personal para las siguientes finalidades secundarias que no son necesarias para el servicio solicitado, pero que nos permiten y facilitan brindarle una mejor atenci&oacute;n:</p>
<p style="text-align: justify; margin-left: 40px;">
    &bull; Para utilizar los datos del hu&eacute;sped y generar un perfil sobre su consumo de servicios de alojamiento temporal:</p>
<ul style="margin-left: 40px;">
    <li style="text-align: justify;">
        RESTAURANTE&nbsp;</li>
    <li style="text-align: justify;">
        TERRAZA BAR</li>
    <li style="text-align: justify;">
        SERVIBAR</li>
    <li style="text-align: justify;">
        LAVANDERIA, TINTORER&Iacute;A Y PLANCHADO</li>
    <li style="text-align: justify;">
        CONCIERGE INTERNA Y EXTERNA</li>
    <li style="text-align: justify;">
        MENSAJERIA</li>
    <li style="text-align: justify;">
        SERVICIO TELEF&Oacute;NICO</li>
    <li style="text-align: justify;">
        VALET PARKING</li>
    <li style="text-align: justify;">
        ENCUESTA DE SERVICIO</li>
    <li style="text-align: justify;">
        SERVICIO M&Eacute;DICO (Consulte m&aacute;s adelante el tratamiento de datos personales sensibles)</li>
    <li style="text-align: justify;">
        ALQUILER DE SALONES PARA EVENTOS</li>
</ul>
<p style="text-align: justify;">
    <strong>&bull; HOTELES CASA INN, S. A. DE C. V., </strong>puede utilizar la informaci&oacute;n personal que nos proporciona para ofrecerle un mejor servicio y, en su caso, identificar sus preferencias durante su estancia y hacerla m&aacute;s placentera;</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>&bull; HOTELES CASA INN, S. A. DE C. V.,</strong> puede utilizar la informaci&oacute;n personal que nos proporciona para operar los servicios y productos alternos al hospedaje, que son adicionales o complementarios y que solicita o contrata con nosotros durante su alojamiento temporal o uso de los servicios alternos del hotel:</p>
<ul style="margin-left: 40px;">
    <li style="text-align: justify;">
        COMPRA DE PAQUETES VACACIONALES</li>
    <li style="text-align: justify;">
        PROGRAMAS DE LEALTAD</li>
    <li style="text-align: justify;">
        RESERVACIONES DE TOURS Y PASEOS AL EXTERIOR DEL HOTEL</li>
    <li style="text-align: justify;">
        ALQUILER DE VEH&Iacute;CULOS DE TRANSPORTE Y SERVICIO DE TAXI</li>
    <li style="text-align: justify;">
        SERVICIO DE ADQUISICI&Oacute;N DE BOLETOS DE AVI&Oacute;N O CUALQUIER OTRO TRANSPORTE, AS&Iacute; COMO RESERVACIONES EN OTROS HOTELES DE LA REP&Uacute;BLICA MEXICANA O DEL EXTRANJERO, A SOLICITUD DEL HU&Eacute;SPED O USUARIO DE LOS SERVICIOS DEL HOTEL</li>
    <li style="text-align: justify;">
        SERVICIO DE ADQUISICI&Oacute;N DE BOLETOS PARA CONFERENCIAS, CURSOS Y EVENTOS A SOLICITUD DEL HU&Eacute;SPED O USUARIO DE LOS SERVICIOS DEL HOTEL</li>
</ul>
<p style="text-align: justify; margin-left: 40px;">
    &bull; Mercadotecnia o Publicidad en relaci&oacute;n a la colecci&oacute;n de Hoteles de nombre comercial CASA INN:</p>
<p style="text-align: justify; margin-left: 80px;">
    <strong>- HOTELES CASA INN, S. A. DE C. V.,</strong> puede utilizar la informaci&oacute;n personal que nos proporciona para ofrecerle promociones y productos tur&iacute;sticos y comerciales en general de los Hoteles de nombre comercial CASA INN, as&iacute; como cualquier otro producto o servicio relacionado como son: promociones servicios especiales, boletines informativos, encuestas y otros productos y servicios de los Hoteles CASA INN.</p>
<p style="text-align: justify; margin-left: 40px;">
    &bull; Finalmente la informaci&oacute;n que nos proporciona podr&aacute; ser tratada en los casos que expresamente autoriza el Art. 10 de la Ley Federal de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de los Particulares.</p>
<p style="text-align: justify;">
    La negativa para el uso de sus datos personales para estas finalidades no podr&aacute; ser un motivo para que le neguemos los servicios y productos que solicita o contrata con nosotros.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>&iquest;Qu&eacute; datos personales utilizaremos para estos fines?</strong></p>
<p style="text-align: justify;">
    Para llevar a cabo las finalidades descritas en el presente Aviso de Privacidad, utilizaremos los siguientes datos personales:</p>
<p style="text-align: justify;">
    &bull; Nombre<br/>
    &bull; Registro Federal de Contribuyentes(RFC)<br/>
    &bull; Ciudad de residencia y/o ciudad de origen<br/>
    &bull; Pa&iacute;s de origen<br/>
    &bull; Fecha de nacimiento<br/>
    &bull; Nacionalidad<br/>
    &bull; Estado Civil<br/>
    &bull; Ocupaci&oacute;n y grado de escolaridad<br/>
    &bull; Domicilio<br/>
    &bull; Tel&eacute;fono particular<br/>
    &bull; Tel&eacute;fono celular<br/>
    &bull; Correo electr&oacute;nico<br/>
    &bull; Compa&ntilde;&iacute;a o empresa que representa<br/>
    &bull; Motivo del viaje o alojamiento<br/>
    &bull; Fecha de entrada y salida del hotel<br/>
    &bull; Datos y placas del veh&iacute;culo propiedad o en posesi&oacute;n<br/>
    &bull; N&uacute;mero de personas que se hospedan o hacen uso del hotel y sus servicios<br/>
    &bull; Forma de pago<br/>
    &bull; En caso de pagar con tarjeta de cr&eacute;dito.- Datos correspondientes de la tarjeta<br/>
    &bull; En caso de pagar con transferencia electr&oacute;nica.- Datos correspondientes a la cuenta bancaria<br/>
    &bull; Firma aut&oacute;grafa</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>&iquest;Con qui&eacute;n compartimos su informaci&oacute;n personal y para qu&eacute; fines?</strong></p>
<p style="text-align: justify;">
    Le informamos que sus datos personales son compartidos dentro del pa&iacute;s con las siguientes personas y empresas, para los siguientes fines:</p>
<table border="0" cellpadding="01" cellspacing="10" style="width: 851px;">
    <tbody>
        <tr>
            <td style="width: 300px; height: 32px;">
                <p style="text-align: center;">
                    <strong>Des</strong><strong>tinatario de los datos personales</strong></p>
            </td>
            <td style="width: 238px; height: 32px;">
                <p style="text-align: center;">
                    <strong>F</strong><strong>i</strong><strong>n</strong><strong>a</strong><strong>l</strong><strong>i</strong><strong>d</strong><strong>a</strong><strong>d</strong></p>
            </td>
            <td style="width: 266px; height: 32px;">
                <p style="text-align: center; margin-right: 0.2pt;">
                    <strong>Re</strong><strong>qui</strong><strong>er</strong><strong>e del</strong><br/>
                    <strong>c</strong><strong>on</strong><strong>se</strong><strong>n</strong><strong>t</strong><strong>i</strong><strong>m</strong><strong>i</strong><strong>e</strong><strong>n</strong><strong>to</strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 300px; height: 77px;">
                <p style="text-align: center; margin-right: 0.2pt;">
                    SOCIEDADES DEL MISMO GRUPO DEL RESPONSABLE QUE OPERE BAJO LOS MISMOS PROCESOS, POLITICAS INTERNAS Y NOMBRES COMERCIALES&nbsp;</p>
            </td>
            <td style="width: 238px; height: 77px;">
                <p style="text-align: center;">
                    INVITAR A LOS HUESPEDES A VISITAR O QUE CONOZCAN NUESTRA COLECCION DE HOTELES Y SERVICIOS</p>
            </td>
            <td style="width: 266px; height: 77px;">
                <p style="text-align: center;">
                    No</p>
            </td>
        </tr>
    </tbody>
</table>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>&iquest;C&oacute;mo puede acceder, rectificar o cancelar sus datos personales, u oponerse a su uso?</strong></p>
<p style="text-align: justify;">
    Usted tiene derecho a conocer qu&eacute; datos personales tenemos de usted, para qu&eacute; los utilizamos y las condiciones del uso que les damos (Acceso); asimismo, es su derecho solicitar la correcci&oacute;n de su informaci&oacute;n personal en caso de que est&eacute; desactualizada, sea inexacta o incompleta (Rectificaci&oacute;n); que la eliminemos de nuestros registros o bases de datos cuando considere que la misma no est&aacute; siendo utilizada adecuadamente (Cancelaci&oacute;n); as&iacute; como oponerse al uso de sus datos personales para fines espec&iacute;ficos (Oposici&oacute;n). Estos derechos se conocen como derechos ARCO.</p>
<p style="text-align: justify;">
    Para el ejercicio de cualquiera de los derechos ARCO, usted deber&aacute; presentar la solicitud respectiva a trav&eacute;s del siguiente medio: LLAMANDO AL NUMERO TELEFONICO 01 (461)5986700 DE LA CIUDAD DE CELAYA; O AL CORREO ELECTR&Oacute;NICO <span id="cloak6582567fe9ef54c61b63740e83819b63">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
                document.getElementById('cloak6582567fe9ef54c61b63740e83819b63').innerHTML = '';
                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                var path = 'hr' + 'ef' + '=';
                var addy6582567fe9ef54c61b63740e83819b63 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;';
                addy6582567fe9ef54c61b63740e83819b63 = addy6582567fe9ef54c61b63740e83819b63 + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx?s&#117;bj&#101;ct=D&#97;t&#111;s%20P&#101;rs&#111;n&#97;l&#101;s%20C&#97;s&#97;%20Inn%20H&#111;t&#101;l&#101;s';
                var addy_text6582567fe9ef54c61b63740e83819b63 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;' + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx';document.getElementById('cloak6582567fe9ef54c61b63740e83819b63').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy6582567fe9ef54c61b63740e83819b63 + '\'>'+addy_text6582567fe9ef54c61b63740e83819b63+'<\/a>';
        </script> o <strong>mediante una solicitud que deber&aacute; ser presentada por escrito con acuse de recibo, al siguiente domicilio: calle Av. Juan Jose Torres Landa No.202 , Fracc. Del Parque, C. P. 38010, en la Ciudad de Celaya, de 9:00 a 14:00 horas de Lunes a Viernes</strong>. Con la finalidad de proteger su confidencialidad, <strong>HOTELES CASA INN, S. A. DE C. V.,</strong> enviar&aacute; la respuesta a su solicitud por escrito al domicilio o al correo electr&oacute;nico que haya proporcionado en su escrito de solicitud, a elecci&oacute;n de <strong>HOTELES CASA INN, S. A. DE C. V.</strong></p>
<p style="text-align: justify;">
    Para conocer el procedimiento y requisitos para el ejercicio de los derechos ARCO, ponemos a su disposici&oacute;n el siguiente medio:</p>
<p style="text-align: justify;">
    LLAMANDO AL NUMERO TELEFONICO 01 (461)5986700 DE LA CIUDAD DE CELAYA; O AL CORREO ELECTR&Oacute;NICO <span id="cloakc88549f8c98c4c620c8001f98bc5fad2">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
                document.getElementById('cloakc88549f8c98c4c620c8001f98bc5fad2').innerHTML = '';
                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                var path = 'hr' + 'ef' + '=';
                var addyc88549f8c98c4c620c8001f98bc5fad2 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;';
                addyc88549f8c98c4c620c8001f98bc5fad2 = addyc88549f8c98c4c620c8001f98bc5fad2 + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx?s&#117;bj&#101;ct=D&#97;t&#111;s%20P&#101;rs&#111;n&#97;l&#101;s%20C&#97;s&#97;%20Inn%20H&#111;t&#101;l&#101;s';
                var addy_textc88549f8c98c4c620c8001f98bc5fad2 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;' + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx';document.getElementById('cloakc88549f8c98c4c620c8001f98bc5fad2').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyc88549f8c98c4c620c8001f98bc5fad2 + '\'>'+addy_textc88549f8c98c4c620c8001f98bc5fad2+'<\/a>';
        </script> o mediante una solicitud que deber&aacute; ser presentada por escrito con acuse de recibo, al siguiente domicilio: calle Av. Juan. Jose Torres Landa No.202 Col Fracc. Del Parque C.P. 38010 , en la Ciudad de Celaya, &nbsp;de 9:00 a 14:00 horas de Lunes a Viernes. Con la finalidad de proteger su confidencialidad, <strong>HOTELES CASA INN, S. A. DE C. V.</strong>, enviar&aacute; la respuesta a su solicitud por escrito al domicilio o al correo electr&oacute;nico que haya proporcionado en su escrito de solicitud, a elecci&oacute;n de <strong>HOTELES CASA INN, S. A. DE C. V.</strong></p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    Los datos de contacto de la persona o departamento de datos personales, que est&aacute; a cargo de dar tr&aacute;mite a las solicitudes de derechos ARCO, son los siguientes:</p>
<p style="text-align: justify;">
    a) Nombre de la persona o departamento de datos personales: DEPARTAMENTO DE DATOS PERSONALES CASA INN.</p>
<p style="text-align: justify;">
    b) Domicilio: Av. Juan. Jose Torres Landa No.202 Col. Fracc. Del Parque C.P. 38010 , en la Ciudad de Celaya, pa&iacute;s M&eacute;xico</p>
<p style="text-align: justify;">
    c) Correo electr&oacute;nico: <span id="cloak8f753b3e686fad8658d8288e7145e9eb">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
                document.getElementById('cloak8f753b3e686fad8658d8288e7145e9eb').innerHTML = '';
                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                var path = 'hr' + 'ef' + '=';
                var addy8f753b3e686fad8658d8288e7145e9eb = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;';
                addy8f753b3e686fad8658d8288e7145e9eb = addy8f753b3e686fad8658d8288e7145e9eb + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx?s&#117;bj&#101;ct=D&#97;t&#111;s%20P&#101;rs&#111;n&#97;l&#101;s%20C&#97;s&#97;%20Inn%20H&#111;t&#101;l&#101;s';
                var addy_text8f753b3e686fad8658d8288e7145e9eb = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;' + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx';document.getElementById('cloak8f753b3e686fad8658d8288e7145e9eb').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy8f753b3e686fad8658d8288e7145e9eb + '\'>'+addy_text8f753b3e686fad8658d8288e7145e9eb+'<\/a>';
        </script></p>
<p style="text-align: justify;">
    d) N&uacute;mero telef&oacute;nico: 01 55 5242 7750</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>Usted puede cancelar su consentimiento para el uso de sus datos personales: </strong></p>
<p style="text-align: justify;">
    Usted puede cancelar el consentimiento que, en su caso, nos haya otorgado para el tratamiento de sus datos personales. Sin embargo, es importante que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligaci&oacute;n legal requiramos seguir tratando sus datos personales. Asimismo, usted deber&aacute; considerar que para ciertos fines, la revocaci&oacute;n de su consentimiento podr&iacute;a implicar que no le podamos seguir prestando el servicio que nos solicit&oacute;, o la conclusi&oacute;n de su relaci&oacute;n con nosotros.</p>
<p style="text-align: justify;">
    Para cancelar su consentimiento deber&aacute; presentar su solicitud a trav&eacute;s del siguiente medio:</p>
<p style="text-align: justify;">
    LLAMANDO AL NUMERO TELEFONICO 01 55 5242 7750 DE LA CIUDAD DE MEXICO; O AL CORREO ELECTR&Oacute;NICO <span id="cloaked017eba4953523f37a34261bc790496">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
                document.getElementById('cloaked017eba4953523f37a34261bc790496').innerHTML = '';
                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                var path = 'hr' + 'ef' + '=';
                var addyed017eba4953523f37a34261bc790496 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;';
                addyed017eba4953523f37a34261bc790496 = addyed017eba4953523f37a34261bc790496 + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx?s&#117;bj&#101;ct=D&#97;t&#111;s%20P&#101;rs&#111;n&#97;l&#101;s%20C&#97;s&#97;%20Inn%20H&#111;t&#101;l&#101;s';
                var addy_texted017eba4953523f37a34261bc790496 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;' + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx';document.getElementById('cloaked017eba4953523f37a34261bc790496').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyed017eba4953523f37a34261bc790496 + '\'>'+addy_texted017eba4953523f37a34261bc790496+'<\/a>';
        </script> o mediante una solicitud que deber&aacute; ser presentada por escrito con acuse de recibo, al siguiente domicilio: Av. Juan. Jose Torres Landa No.202 Col Fracc. Del Parque C.P. 38010 , en la Ciudad de Celaya, de 9:00 a 14:00 horas de Lunes a Viernes. Con la finalidad de proteger su confidencialidad, <strong>HOTELES CASA INN, S. A. DE C. V.,</strong> enviar&aacute; la respuesta a su solicitud por escrito al domicilio o al correo electr&oacute;nico que haya proporcionado en su escrito de solicitud, a elecci&oacute;n de <strong>HOTELES CASA INN, S. A. DE C. V.</strong></p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    Para conocer el procedimiento y requisitos para la cancelaci&oacute;n del consentimiento, ponemos a su disposici&oacute;n el siguiente medio:</p>
<p style="text-align: justify;">
    LLAMANDO AL NUMERO TELEFONICO 01 55 5242 7750 DE LA CIUDAD DE MEXICO; O AL CORREO ELECTR&Oacute;NICO <span id="cloak37186494c463f457fdee8df009bfe485">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
                document.getElementById('cloak37186494c463f457fdee8df009bfe485').innerHTML = '';
                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                var path = 'hr' + 'ef' + '=';
                var addy37186494c463f457fdee8df009bfe485 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;';
                addy37186494c463f457fdee8df009bfe485 = addy37186494c463f457fdee8df009bfe485 + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx?s&#117;bj&#101;ct=D&#97;t&#111;s%20P&#101;rs&#111;n&#97;l&#101;s%20C&#97;s&#97;%20Inn%20H&#111;t&#101;l&#101;s';
                var addy_text37186494c463f457fdee8df009bfe485 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;' + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx';document.getElementById('cloak37186494c463f457fdee8df009bfe485').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy37186494c463f457fdee8df009bfe485 + '\'>'+addy_text37186494c463f457fdee8df009bfe485+'<\/a>';
        </script> o mediante una solicitud que deber&aacute; ser presentada por escrito con acuse de recibo, al siguiente domicilio: calle&nbsp;Av. Juan. Jose Torres Landa No.202 Col Fracc. Del Parque C.P. 38010 , en la Ciudad de Celaya, de 9:00 a 14:00 horas de Lunes a Viernes. Con la finalidad de proteger su confidencialidad, <strong>HOTELES CASA INN, S. A. DE C. V.,</strong> enviar&aacute; la respuesta a su solicitud por escrito al domicilio o al correo electr&oacute;nico que haya proporcionado en su escrito de solicitud, a elecci&oacute;n de <strong>HOTELES CASA INN, S. A. DE C. V.</strong></p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>&iquest;C&oacute;mo puede limitar el uso o divulgaci&oacute;n de su informaci&oacute;n personal? </strong></p>
<p style="text-align: justify;">
    Con objeto de que usted pueda limitar el uso y divulgaci&oacute;n de su informaci&oacute;n personal, le ofrecemos los siguientes medios:</p>
<p style="text-align: justify;">
    LLAMANDO AL NUMERO TELEFONICO 01 55 5242 7750 DE LA CIUDAD DE MEXICO; O AL CORREO ELECTR&Oacute;NICO <span id="cloak3cf8e863ce7feab912b7a4742e35e320">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
                document.getElementById('cloak3cf8e863ce7feab912b7a4742e35e320').innerHTML = '';
                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                var path = 'hr' + 'ef' + '=';
                var addy3cf8e863ce7feab912b7a4742e35e320 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;';
                addy3cf8e863ce7feab912b7a4742e35e320 = addy3cf8e863ce7feab912b7a4742e35e320 + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx';
                var addy_text3cf8e863ce7feab912b7a4742e35e320 = 'd&#97;t&#111;sp&#101;rs&#111;n&#97;l&#101;s' + '&#64;' + 'c&#97;s&#97;&#105;nn' + '&#46;' + 'c&#111;m' + '&#46;' + 'mx';document.getElementById('cloak3cf8e863ce7feab912b7a4742e35e320').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy3cf8e863ce7feab912b7a4742e35e320 + '\'>'+addy_text3cf8e863ce7feab912b7a4742e35e320+'<\/a>';
        </script> o mediante una solicitud que deber&aacute; ser presentada por escrito con acuse de recibo, al siguiente domicilio:&nbsp;Av. Juan. Jose Torres Landa No.202 Col Fracc. Del Parque C.P. 38010 , en la Ciudad de Celaya, de 9:00 a 14:00 horas de Lunes a Viernes. Con la finalidad de proteger su confidencialidad, <strong>HOTELES CASA INN, S. A. DE C. V.,</strong> enviar&aacute; la respuesta a su solicitud por escrito al domicilio o al correo electr&oacute;nico que haya proporcionado en su escrito de solicitud, a elecci&oacute;n de <strong>HOTELES CASA INN, S. A. DE C. V.</strong></p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>&iquest;C&oacute;mo puede conocer los cambios en este Aviso de Privacidad? </strong></p>
<p style="text-align: justify;">
    El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los productos o servicios que ofrecemos; de nuestras pr&aacute;cticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas.</p>
<p style="text-align: justify;">
    Nos comprometemos a mantenerlo informado sobre los cambios que pueda sufrir el presente aviso de privacidad, a trav&eacute;s de: <a href="http://www.casainn.com.mx">www.casainn.com.mx</a></p>
<p style="text-align: justify;">
    El procedimiento a trav&eacute;s del cual se llevar&aacute;n a cabo las notificaciones sobre cambios o actualizaciones al presente Aviso de Privacidad es el siguiente:</p>
<p style="text-align: justify;">
    EN LA PAGINA WEB <a href="http://www.casainn.com.mx">www.casainn.com.mx</a> en la secci&oacute;n AVISO DE PRIVACIDAD o, a trav&eacute;s del Aviso de Privacidad Simplificado ubicado en las instalaciones de <strong>HOTELES CASA INN, S. A. DE C. V.</strong> Por lo anterior, le sugerimos visitar peri&oacute;dicamente este Aviso de Privacidad para estar enterado de cualquier actualizaci&oacute;n.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>Uso de Datos Personales Sensibles. </strong></p>
<p style="text-align: justify;">
    Se consideran datos personales sensibles aquellos datos personales que afecten a la esfera m&aacute;s &iacute;ntima de su titular, o cuya utilizaci&oacute;n indebida pueda dar origen a discriminaci&oacute;n o conlleve un riesgo grave para &eacute;ste. En particular, se consideran sensibles aquellos que puedan revelar aspectos como origen racial o &eacute;tnico, estado de salud presente y futuro, informaci&oacute;n gen&eacute;tica, creencias religiosas, filos&oacute;ficas y morales, afiliaci&oacute;n sindical, opiniones pol&iacute;ticas, preferencias sexuales y dicha informaci&oacute;n s&oacute;lo es recopilada por <strong>HOTELES CASA INN, S. A. DE C. V.,</strong> de los hu&eacute;spedes y/o usuarios y podr&aacute; ser utilizada para mejorar o satisfacer las necesidades de los mismos y los servicios que se les proporcionan.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>Control y Seguridad de informaci&oacute;n personal. </strong></p>
<p style="text-align: justify;">
    <strong>HOTELES CASA INN, S. A. DE C. V.,</strong> se compromete a tomar las medidas necesarias para proteger la informaci&oacute;n recopilada, utilizando tecnolog&iacute;as de seguridad y procedimientos de control en el acceso, uso o divulgaci&oacute;n de su informaci&oacute;n personal sin autorizaci&oacute;n, por ejemplo, almacenando la informaci&oacute;n personal proporcionada en servidores ubicados en Centros de Datos que cuentan con controles de acceso limitado. Para transacciones en l&iacute;nea, utilizamos tambi&eacute;n tecnolog&iacute;as de seguridad que protegen la informaci&oacute;n personal que nos sea transmitida a trav&eacute;s de los diversos medios electr&oacute;nicos. Sin embargo, ning&uacute;n sistema de seguridad o de transmisi&oacute;n de datos del cual la empresa no tenga el control absoluto y/o tenga dependencia con internet puede garantizar que sea totalmente seguro.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>Medios electr&oacute;nicos y cookies. </strong></p>
<p style="text-align: justify;">
    En el supuesto de que usted utilice medios electr&oacute;nicos en relaci&oacute;n a sus datos personales se generar&aacute;n a efecto de proporcionarle un mejor servicio cookies. Los cookies son peque&ntilde;as piezas de informaci&oacute;n que son enviadas por el sitio Web a su navegador.</p>
<p style="text-align: justify;">
    Los cookies se almacenan en el disco duro de su equipo y se utilizan para determinar sus preferencias cuando se conecta a los servicios de nuestros sitios, as&iacute; como para rastrear determinados comportamientos o actividades llevadas a cabo por usted dentro de nuestros sitios.</p>
<p style="text-align: justify;">
    En algunas secciones de nuestro sitio requerimos que el cliente tenga habilitados los cookies ya que algunas de las funcionalidades requieren de &eacute;stas para trabajar. Los cookies nos permiten: a) reconocerlo al momento de entrar a nuestros sitios y ofrecerle de una experiencia personalizada, b) conocer la configuraci&oacute;n personal del sitio especificada por usted, por ejemplo, los cookies nos permiten detectar el ancho de banda que usted ha seleccionado al momento de ingresar al home page de nuestros sitios, de tal forma que sabemos qu&eacute; tipo de informaci&oacute;n es aconsejable descargar, c) calcular el tama&ntilde;o de nuestra audiencia y medir algunos par&aacute;metros de tr&aacute;fico, pues cada navegador que obtiene acceso a nuestros sitios adquiere un cookie que se usa para determinar la frecuencia de uso y las secciones de los sitios visitadas, reflejando as&iacute; sus h&aacute;bitos y preferencias, informaci&oacute;n que nos es &uacute;til para mejorar el contenido, los titulares y las promociones para los usuarios. Los cookies tambi&eacute;n nos ayudan a rastrear algunas actividades, por ejemplo, en algunas de las encuestas que lanzamos en l&iacute;nea, podemos utilizar cookies para detectar si el usuario ya ha llenado la encuesta y evitar desplegarla nuevamente, en caso de que lo haya hecho. El bot&oacute;n de &quot;ayuda&quot; que se encuentra en la barra de herramientas de la mayor&iacute;a de los navegadores, le dir&aacute; c&oacute;mo evitar aceptar nuevos cookies, c&oacute;mo hacer que el navegador le notifique cuando recibe un nuevo cookie o c&oacute;mo deshabilitar todos los cookies. Sin embargo, las cookies le permitir&aacute;n tomar ventaja de las caracter&iacute;sticas m&aacute;s ben&eacute;ficas que le ofrecemos, por lo que le recomendamos que las deje activadas.</p>
<p>
    <strong>&Uacute;ltima actualizaci&oacute;n: 14/06/2016</strong></p>
    </div>

@endsection

@section('javascripts')
    <script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("home");
        d.className += " active";
    });
</script>
@endsection
