@extends('layouts.appveleros')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('veleros.suites')}}</h3>
        </div>

    <!-- =========== MAIN ========== -->
        <main id="room_page">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="slider">
                            <div id="slider-larg" class="owl-carousel owl-demo">
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/veleros/suit01.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/veleros/suit02.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/veleros/suit03.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/veleros/suit04.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/veleros/suit05.jpg" alt="Image">
                                </div>
                                <!-- ITEM -->
                                <div class="item lightbox-image-icon">
                                    <img class="img-responsive" src="hotel/images/veleros/suit06.jpg" alt="Image">
                                </div>
                            </div>
                            <div id="thumbs" class="owl-carousel">
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/veleros/suit001.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/veleros/suit002.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/veleros/suit003.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/veleros/suit004.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/veleros/suit005.jpg" alt="Image"></div>
                                <!-- ITEM -->
                                <div class="item"><img class="img-responsive" src="hotel/images/veleros/suit006.jpg" alt="Image"></div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">

                        <div class="t_style a_left s_title mt50">
                            <p class="text-center">{{ trans('veleros.suitesdes')}}</p>
                            <div class="c_inner mt50">
                                <h2 class="c_title">{{ trans('veleros.serviciossuites')}}</h2>
                            </div>
                        </div>

                        <div class="room_facilitys_list">
                            <div class="all_facility_list">
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-phone"></i>{{ trans('veleros.2tel')}}</li>
                                        <li><i class="fa fa-snowflake-o"></i>{{ trans('veleros.aire')}}</li>
                                        <li><i class="fa fa-lock"></i>{{ trans('veleros.seguridad')}}</li>
                                        <li><i class="fa fa-key"></i>{{ trans('veleros.chapas')}}</li>
                                        <li><i class="fa fa-briefcase"></i>{{trans('veleros.escritorio')}}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-cutlery"></i>{{ trans('veleros.frigobar')}}</li>
                                        <li><i class="fa fa-wifi"></i>{{ trans('veleros.wifi') }}</li>
                                        <li><i class="fa fa-shower"></i>{{ trans('veleros.kitamenidades')}}</li>
                                        <li><i class="fa fa-black-tie"></i>{{ trans('veleros.kitplanchado')}}</li>
                                        <li><i class="fa fa-lightbulb-o"></i>{{ trans('veleros.lamparas')}}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-television"></i>{{ trans('veleros.pantallaled')}}</li>
                                        <li id="planos"><i class="fa fa-map"></i>{{ trans('veleros.planos') }}</li>
                                        <li><i class="fa fa-clock-o"></i>{{trans('veleros.despertador')}}</li>
                                        <li><i class="fa fa-volume-up"></i>{{ trans('veleros.sonido')}}</li>
                                        <li><i class="fa fa-fire-extinguisher"></i>{{ trans('veleros.incendios')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div id="myModal" class="modal">
                            <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
                            <img class="modal-content" id="img01">
                            <div id="caption"></div>
                        </div>

                        <div class="col-md-4 col-md-offset-4 mt40">
                            <a href="/veleros" class="button  btn_blue btn_full upper">{{ trans('menu.reserva') }}</a>
                        </div>
                    </div>

                </div>
            </div>
        </main>

@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("rooms");
        d.className += " active";

        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var text = document.getElementById('planos');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        text.onclick = function(){
            modal.style.display = "block";
            modalImg.src = "hotel/images/veleros/Veleros_Suite.jpg";
            captionText.innerHTML = "{{ trans('veleros.suites')}}";
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    });
</script>
@endsection