@extends('layouts.appveleros')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title">
            <h3 class="upper">{{ trans('menu.habanner')}}</h3>
        </div>

    <!-- =========== MAIN ========== -->
        <main id="rooms_list">
           <div class="container">

              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/juniorsuite-veleros" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/veleros/junior.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/juniorsuite-veleros"> {{ trans('veleros.junior')}} </a></h3>
                             <p class="text-justify">{{ trans('veleros.textojr')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i>
                                <i class="fa fa-cutlery"></i>
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/veleros" class="button  btn_blue btn_full upper">{{ trans('veleros.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>

              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/deluxematrimonial-veleros" class="hover_effect h_link h_yellow">
                          <img src="hotel/images/veleros/estandar.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/deluxematrimonial-veleros"> {{ trans('veleros.estandarmat')}} </a></h3>
                             <p class="text-justify">{{ trans('veleros.textoestandar')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i> 
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i>
                                <i class="fa fa-cutlery"></i> 
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/veleros" class="button  btn_blue btn_full upper">{{ trans('veleros.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>

              <!-- ITEM-->
              <article class="room_list">
                 <div class="row row-flex">
                    <div class="col-lg-4 col-md-5 col-sm-12">
                       <figure>
                          <a href="/suites-veleros" class="hover_effect h_link h_yellow">
                             <img src="hotel/images/veleros/suite.jpg" class="img-responsive" alt="Image">
                          </a>
                       </figure>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12">
                       <div class="room_details row-flex">
                          <div class="col-md-9 col-sm-9 col-xs-12 room_desc">
                             <h3><a href="/suites-veleros"> {{ trans('veleros.suites')}} </a></h3>
                             <p class="text-justify">{{ trans('veleros.textosuite1')}}</p>
                             <div class="room_services">
                                <i class="fa fa-wifi"></i>
                                <i class="fa fa-coffee"></i>
                                <i class="fa fa-television"></i>
                                <i class="fa fa-cutlery"></i>
                             </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 room_price">
                             <div class="room_price_inner">
                                <a href="/veleros" class="button  btn_blue btn_full upper">{{ trans('veleros.reservar')}}</a>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </article>

            </div>
        </main>


@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("rooms");
        d.className += " active";
    });
</script>
@endsection