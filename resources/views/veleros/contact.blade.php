@extends('layouts.appveleros')

@section('content')
    <!-- =========== GOOGLE MAP ========== -->
        <div id="map">
            <div id="google-map">
            </div>
            <div id="map-canvas"></div>
        </div>

        <!-- ========== MAIN ========== -->
        <main id="contact_page">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-8">
                        <div class="main_title a_left">
                            <h2>{{ trans('veleros.contactanos')}}</h2>
                        </div>
                        <form id="contact-form-page">
                            <div class="row">
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('veleros.nombre')}}</label>
                                    <input type="text" class="form-control" name="name" placeholder="{{ trans('veleros.nombre')}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('veleros.telefono')}}</label>
                                    <input type="text" class="form-control" name="phone" placeholder="{{ trans('veleros.telefono')}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('veleros.correo')}}</label>
                                    <input type="email" class="form-control" name="email" placeholder="{{ trans('veleros.correo')}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <label class="control-label">{{ trans('veleros.asunto')}}</label>
                                    <input type="text" class="form-control" name="subject" placeholder="{{ trans('veleros.asunto')}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">{{ trans('veleros.mensaje')}}</label>
                                    <textarea class="form-control" name="message" placeholder="{{ trans('veleros.mensaje')}}"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <button type="submit" class="button  btn_blue mt40 upper pull-right">
                                        <i class="fa fa-paper-plane-o" aria-hidden="true"></i> {{ trans('veleros.enviar')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="main_title a_left">
                            <h2>{{ trans('veleros.datos')}}</h2>
                        </div>
                        <ul class="contact-info">
                            <li>
                                <span>{{ trans('veleros.direccion')}}:</span> Av. Juan José Torres Landa 202,<br/> Fracc del Parque, Celaya,Gto

                            </li>
                            <li>
                                <span>{{ trans('veleros.correo')}}:</span> ventas1cel@casainn.com.mx
                            </li>
                            <li>
                                <span>{{ trans('veleros.telefono')}}:</span> 01 (461)<strong>206 8637</strong>
                            </li>
                        </ul>
                        <div class="social_media sm_contact">
                            <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                            <a class="youtube" href="#"><i class="fa fa-youtube"></i></a>
                            <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                            <a class="googleplus" href="#"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </main>

@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("contact");
        d.className += " active";
    });
</script>

<script type="text/javascript">
/*========== GOOGLE MAP ==========*/
        function initialize() {
            var map;
            var panorama;
            //20.535837, -100.830984
            var var_latitude = 20.535837; // Google Map Latitude
            var var_longitude = -100.830984; // Google Map Longitude
            var pin = '../hotel/images/icons/pin.png';

            //Map pin-window details
            var title = "Casa inn - Querétaro";
            var hotel_name = "Casa inn - Querétaro";
            var hotel_address = "Paseo de Miranda Ote 2,Fracc Monte Miranda Querétaro";
            var hotel_desc = "Hotel";

            var hotel_location = new google.maps.LatLng(var_latitude, var_longitude);
            var mapOptions = {
                center: hotel_location,
                zoom: 15,
                scrollwheel: false, 
                streetViewControl: false
            };
            map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
            var contentString =
                '<div id="infowindow_content">' +
                '<p><strong>' + hotel_name + '</strong><br>' +
                hotel_address + '<br>' +
                hotel_desc + '</p>' +
                '</div>';

            var var_infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            var marker = new google.maps.Marker({
                position: hotel_location,
                map: map,
                icon: pin,
                title: title,
                maxWidth: 500,
                optimized: false,
            });
            google.maps.event.addListener(marker, 'click', function () {
                var_infowindow.open(map, marker);
            });
        }

        //Check if google map div exist
        if ($("#map-canvas").length > 0){
           google.maps.event.addDomListener(window, 'load', initialize);
        }
</script>
@endsection