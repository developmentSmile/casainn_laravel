@extends('layouts.appveleros')

@section('styles')
    <link href="hotel/revolution/css/layers.css" rel="stylesheet" type="text/css" />
    <link href="hotel/revolution/css/settings.css" rel="stylesheet" type="text/css" />
    <link href="hotel/revolution/css/navigation.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
        <!-- ========== REVOLUTION SLIDER ========== -->
            <div id="classic_slider" class="rev_slider" style="display:none">
                <ul>
                    <!-- SLIDE NR. 1 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/veleros/Slider03_veleros.jpg" 
                             alt="Image" 
                             title="slider_bg2"
                             data-bgposition="center center" 
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat" 
                             data-bgparallax="10" 
                             class="rev-slidebg" 
                             data-no-retina="">
                    </li>

                    <!-- SLIDE NR. 2 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/veleros/Slider01_veleros.jpg" 
                             alt="Image" 
                             title="slider_bg2"
                             data-bgposition="center center" 
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat" 
                             data-bgparallax="10" 
                             class="rev-slidebg" 
                             data-no-retina="">
                    </li>
                    
                    <!-- SLIDE NR. 3 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/veleros/Slider04_veleros.jpg"  
                             alt="Image" 
                             title="slider_bg3"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" 
                             class="rev-slidebg"
                             data-no-retina="">
                    </li>

                    <!-- SLIDE NR. 4 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/veleros/Slider02_veleros.jpg"  
                             alt="Image" 
                             title="slider_bg4"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" 
                             class="rev-slidebg"
                             data-no-retina="">
                    </li>

                    <!-- SLIDE NR. 5 -->
                    <li data-transition="crossfade">
                        <!-- MAIN IMAGE -->
                        <img src="hotel/images/veleros/Slider05_veleros.jpg"  
                             alt="Image" 
                             title="slider_bg5"
                             data-bgposition="center center"
                             data-bgfit="cover" 
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" 
                             class="rev-slidebg"
                             data-no-retina="">
                    </li>
                    
                </ul>
            </div>
        
            @include('layouts.bookingForm')

            <!-- ========== BIENVENIDO  ========== -->
            <section class="white_bg" id="rooms">
                <div class="container">

                    <div class="main_title mt_wave a_center">
                        <h2>{{ trans('veleros.titulobienvenido')}}</h2>
                    </div>
                    <p class="main_description a_center">{{ trans('veleros.textobienvenido')}}</p>

                    <div class="row"> 
                            <div class="col-md-4">
                                <article class="room">
                                    <figure>
                                        <img src="hotel/images/veleros/BIENVENIDOS_01.jpg" class="img-responsive" alt="Image">
                                    </figure>
                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="room">
                                    <figure>
                                        <img src="hotel/images/veleros/BIENVENIDOS_02.jpg" class="img-responsive" alt="Image">
                                    </figure>
                                </article>
                            </div>
                            <div class="col-md-4">
                                <article class="room">
                                    <figure>
                                       <img src="hotel/images/veleros/BIENVENIDOS_03.jpg" class="img-responsive" alt="Image">
                                    </figure>
                                </article>
                            </div>
                        </div>
                </div>
            </section>

            <!--IMAGEN-->
            <section id="divisor">
                <div class="col-md-4 col-md-offset-4">
                    <a href="/veleros" class="button  btn_blue btn_full upper">{{ trans('menu.reserva') }}</a>
                </div>
            </section>

            <!-- =========== GALERIA ========== -->
            <section id="gallery">
                <div class="container">
                    <div class="main_title mt_wave a_center">
                        <h2>{{ trans('veleros.titulogaleria')}}</h2>
                    </div>
                    <div class="row">
                        <div class="grid gallery_items">
                            <!-- ITEM 1 -->
                            <figure class="g_item col-md-4 col-sm-6 g_swimming_pool">
                                <img src="./hotel/images/veleros/GALERIA01.jpg" class="img-responsive" alt="Image">
                               
                            </figure>
                            <!-- ITEM 2-->
                            <figure class="g_item col-md-4 col-sm-6  g_swimming_pool">
                                <img src="hotel/images/veleros/GALERIA02.jpg" class="img-responsive" alt="Image">
                               
                            </figure>
                            <!-- ITEM 3-->
                            <figure class="g_item col-md-4 col-sm-6 g_restaurant">
                                <img src="hotel/images/veleros/GALERIA03.jpg" class="img-responsive" alt="Image">
                                
                            </figure>
                            <!-- ITEM 4-->
                            <figure class="g_item col-md-4 col-sm-6 g_restaurant">
                                <img src="hotel/images/veleros/GALERIA04.jpg" class="img-responsive" alt="Image">
                                
                            </figure>
                            <!-- ITEM 5-->
                            <figure class="g_item col-md-4 col-sm-6 g_spa">
                                <img src="hotel/images/veleros/GALERIA05.jpg" class="img-responsive" alt="Image">
                                
                            </figure>
                            <!-- ITEM 6-->
                            <figure class="g_item col-md-4 col-sm-6 g_restaurant">
                                <img src="hotel/images/veleros/GALERIA06.jpg" class="img-responsive" alt="Image">
                               
                            </figure>
                            <!-- ITEM 7-->
                            <figure class="g_item col-md-4 col-sm-6 g_island">
                                <img src="hotel/images/veleros/GALERIA07.jpg" class="img-responsive" alt="Image">
                             
                            </figure>
                            <!-- ITEM 8-->
                            <figure class="g_item col-md-4 col-sm-6 g_island">
                                <img src="hotel/images/veleros/GALERIA08.jpg" class="img-responsive" alt="Image">
                               
                            </figure>
                            <!-- ITEM 9-->
                            <figure class="g_item col-md-4 col-sm-6 g_island">
                                <img src="hotel/images/veleros/GALERIA09.jpg" class="img-responsive" alt="Image">
                               
                            </figure>
                        </div>
                    </div>
                </div>
            </section>


@endsection

@section('javascripts')
<!-- ========== REVOLUTION SLIDER ========== -->
    <script type="text/javascript" src="hotel/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="hotel/revolution/js/extensions/revolution.extension.video.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("home");
        d.className += " active";
    });
</script>
@endsection