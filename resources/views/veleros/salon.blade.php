@extends('layouts.appveleros')

@section('content')
    <!-- =========== PAGE TITLE ========== -->
        <div class="page_title2">
            <img src="hotel/images/veleros/BANNER_SALONES.jpg" class="img-responsive">
        </div>

    <!-- ========== MAIN ========== -->
        <main id="events_page">
            <div class="container">

                <!-- ITEM CELAYA-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 500 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('veleros.celaya')}}</h5>
                                <div class="meta">
                                    <div class="time">72 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('veleros.descelaya')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('veleros.montajecel')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/veleros/Gran_Salon_veleros.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM CELAYAS-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 150 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('veleros.celayas')}}</h5>
                                <div class="meta">
                                    <div class="time">72 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('veleros.descelayas')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('veleros.montajecels')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/veleros/Gran_Salon_veleros.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM TRESG-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 50 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('veleros.tresg')}}</h5>
                                <div class="meta">
                                    <div class="time">72 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('veleros.destresg')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('veleros.montajetresg')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/veleros/Gran_Salon_veleros.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM TRESGS-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 25 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('veleros.tresgs')}}</h5>
                                <div class="meta">
                                    <div class="time">72 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('veleros.destresgs')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('veleros.montajetresgs')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/veleros/Gran_Salon_veleros.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM-->
                <div class="item">
                    <div class="row">
                        <div class="col-lg-2 col-md-1">
                            <div class="time-from">
                                <div class="date"> 6 </div>
                                <div class="month">{{ trans('queretaro.personas')}}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="event-wrapper">
                                <h5>{{ trans('veleros.centro')}}</h5>
                                <div class="meta">
                                    <div class="time">72 {{ trans('queretaro.m2')}}</div>
                                    
                                </div>
                                <div class="description">
                                    <p>{{ trans('veleros.descentro')}}</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> {{ trans('veleros.fax')}}</li>
                                        <li><i class="fa fa-check"></i> {{ trans('veleros.impresoras')}}</li>
                                        <li><i class="fa fa-check"></i> {{ trans('veleros.internet')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <div class="image">
                                <img src="hotel/images/veleros/Gran_Salon_veleros.jpg" class="img-responsive" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-4 col-md-offset-4 mt40">
                    <a href="/contact-veleros" class="button  btn_blue btn_full upper">{{ trans('menu.cotiza') }}</a>
                </div>
            </div>
        </main>

@endsection

@section('javascripts')
<script type="text/javascript">
    $(document).ready(function () {
        var d = document.getElementById("salon");
        d.className += " active";
    });
</script>
@endsection